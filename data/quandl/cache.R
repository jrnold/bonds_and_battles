library("Quandl")
library("stringr")

#' Download three series from Quandl from Macaulay (1938) "The Movement Of Interest Rates, Bond Yields, And Stock Prices In The United States Since 1856"
#' These are the standard short and long-run interest series for the Civil War Era,
#' e.g. see Homer and Sylla.
#'

cache_quandl <- function(code, path, ...) {
  .data <- Quandl(code, ...)
  dir.create(path, showWarnings = FALSE)
  code2 <- str_split_fixed(code, "/", 2)
  dir.create(file.path(path, code2[1]), showWarnings = FALSE)
  saveRDS(.data, file.path(path, code2[1], paste0(code2[2], ".rds")))
  cat("cached ", code, "\n")
}

cache_dir <- "cache"

#' [Railroad Bond Yields Index for US](http://www.quandl.com/FRED/M1319AUSM156NNBR")
cache_quandl("FRED/M1319AUSM156NNBR", cache_dir)
#' [Municipal Bond Yields for New England](http://www.quandl.com/FRED/Q13020USQ156NNBR)
cache_quandl("FRED/Q13020USQ156NNBR", cache_dir)
#' [Call money rates, mixed collateral ](http://www.quandl.com/FRED/M13001USM156NNBR)
cache_quandl("FRED/M13001USM156NNBR", cache_dir)
#' [U.S. Call money rates](http://www.quandl.com/FRED/M1301AUSM156NNBR)
cache_quandl("FRED/M1301AUSM156NNBR", cache_dir)
#' [U.S. Index of Wholesale Prices, Variable Group Weights 01/1850-12/1894](https://www.quandl.com/data/NBER/M04048A)
cache_quandl("NBER/M04048A", cache_dir)
#' U.S. Index of the General Price Level 01/1860-11/1939
cache_quandl("NBER/M04051", cache_dir)
