Suppose that $\beta \sim N(\mu, \sigma^2) N(\nu, \tau^2)$.
This comes up in situations similar to fused lasso with shrinkage to 0, and even elastic net.
Neither of those involve two normal distributions, but both involve LASSO, and the Laplace distribution can be represented as a mixture of normals.

Then,
$$
\begin{aligned}[t]
\beta &\sim N( \gamma, \zeta) \\
\end{aligned}
$$
where
$$
\begin{aligned}[t]
\gamma
&= \left( \frac{\frac{1}{\sigma^2} \mu + \frac{1}{\tau^2} \nu }{\frac{1}{\sigma^2} + \frac{1}{\tau^2}} \right) \\
&= \left( \frac{\frac{\sigma^2 \tau^2}{\sigma^2 + \tau^2} \mu + \frac{\sigma^2 \tau^2}{\sigma^2 + \tau^2} \nu }{\frac{\sigma^2 + \tau^2}{\sigma^2 \tau^2}} \right) \\
&= \left( \frac{\tau^2}{\sigma^2 + \tau^2} \mu + \frac{\sigma^2}{\sigma^2 + \tau^2} \nu  \right) \\
\end{aligned}
$$
and
$$
\begin{aligned}[t]
\zeta
&= \left( \frac{1}{\sigma^2} + \frac{1}{\tau^2} \right)^{-1} \\
&= \frac{\sigma^2 \tau^2}{\sigma^2 + \tau^2}
\end{aligned}
$$

Let $\rho = \frac{\tau^2}{\sigma^2}$, then
$$
\begin{aligned}[t]
\beta
&\sim \left( \rho \mu + (1 - \rho) \nu, \rho \sigma^2  \right) 
\end{aligned}
$$


Example:
Consider a case in which a parameter is shrunk to its previous value and to zero, $\beta_t \sim N(\beta_{t-1}, \sigma^2) N(0, \tau^2)$.
Then $\beta_t \sim N(\rho \beta_{t-1}, \rho \sigma^2)$.

Example:
Consider a case in which a parameter is shrunk to its previous value and to its group mean $\bar{\beta}$, $\beta_t \sim N(\beta_{t-1}, \sigma^2) N(\bar{\beta}, \tau^2)$.
Then $\beta_t \sim N(\rho \beta_{t-1} + (1 - \rho) \bar{\beta}, \rho \sigma^2)$.
