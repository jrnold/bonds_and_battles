---
title: Notes on Statistical Models
---

# `model1`

- Data: "Fives of 1874". Daily
- Random walk

  - t-distributed system errors
  - normally distributed observation errors

# `model1a`

- Data: "Fives of 1874".
- Marginalized over any missing values
- Random walk

  - t-distributed system errors
  - normally distributed observation errors

# `model2`

- Data: "Fives of 1874"
- Observation equation: normally distributed
- System equation:

  - t-distributed errors
  - Avg effect of battles
