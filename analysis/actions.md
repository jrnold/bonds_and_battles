# Relevant Events

- Debt Acts
- Treasury Reports
- Emancipation Proclamation
- National Bank Acts of 1863, 1864
- Revenue Acts of 1861, 1862
- Secretary of Treasury
- Secretary of Defense Changes
- General in Chief
- Habeas corpus suspension Act 1863 (12 Stat 75)

# Emancipation Proclamation

[Emancipation Proclamation](http://en.wikipedia.org/wiki/Emancipation_Proclamation) Sep 22, 1863. News on 1863-9-23.
A proclamation by the president of the united states. (1862, Sep 23). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91671117?accountid=13567

# Enrollment Act

See https://en.wikipedia.org/wiki/Enrollment_Act

- 12 Stat 731, enacted Mar 3, 1863. "Civli War Military Draft Act"
- Bill S. 511 A BILL PROVIDING FOR ENROLLING AND CALLING OUT THE NATIONAL FORCES, AND FOR OTHER PURPOSES. (P.1235-1)
- Closest votes was on Feb 23, 1863.  https://www.govtrack.us/congress/votes/37-3/h562 61/60/58

# Militia Act of 1862

- 12 Stat 597 enacted on July 17, 1862.
- S 384. Sentate act.
- Rejected on July 10 https://www.govtrack.us/congress/votes/37-2/s507 and passed on July 15, 1862 https://www.govtrack.us/congress/votes/37-2/s534.

# Second Confiscation Act

- https://en.wikipedia.org/wiki/Second_Confiscation_Act
- Passed on July 17, 1862. Over
- Passed house on June 28 https://www.govtrack.us/congress/votes/37-2/s463 (57 / 13/ 7) May 26, 1862. Later some ammendments.
- H.R. 471 (12 Stat 589)

# First Confiscation Act

Signed by Lincoln on Aug 6, 1861. Seemed to have little effect.

# Draft Riots

July 13-16, 1863.

# Acts

## National Bank Act of 1864

ch. 106, 13 Stat. 99; June 3, 1864 "The National Bank Act of 1864. An Act to Provide a National Currency, secured by a pledge of United States Bonds, and to provide for the circulation and redemption thereof"
.
- Passed H.R. 395: https://www.govtrack.us/congress/votes/38-1/h214. https://www.govtrack.us/congress/votes/38-1/h214 April 18, 1864. Votes of 80/66, 59/76
- HR 333: Votes on April 6, 1864. Some of them close, e.g. https://www.govtrack.us/congress/votes/38-1/h183 (65/63/53)

## Revenue Act of 1864

- Passed on June 30, 1864. 13 Stat. 223.
- Bill was H.R. 405.  Most votes on April 28, 1864. https://www.govtrack.us/congress/votes#session=104 Most of the
  votes appear to be on ammendments.

##  National Bank Act of 1863

Hard to distinguish in the contemporary press. I think it was referred to as the Bank Tax Clause.

- Signed into law on  Feb 25, 1863. ch. 58, 12 Stat. 665, February 25, 1863 http://memory.loc.gov/cgi-bin/ampage?collId=llsl&fileName=012/llsl012.db&recNum=696
- Final acts don't appear to be until March 3, 1863
- Disagreement between House and Senate over the bill. IMPORTANT FROM WASHINGTON. (1863, Feb 27). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91800204?accountid=13567
- Compromise not until March 2, 1863. NEWS FROM WASHINGTON. (1863, Mar 02). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91757820?accountid=13567
- Senate: S 486

    - http://memory.loc.gov/cgi-bin/ampage?collId=llsb&fileName=037/llsb037.db&recNum=1673.p
    - reading of S 486 on Feb 20.
    - Major vote was on Feb 12, 1863 https://www.govtrack.us/congress/votes/37-3/s668. Close 23/21/4
    - Other votes on Feb 10 and 11.	

- H.R. 693: on Introduced January 20, 1863

## Revenue Act of 1862

See http://en.wikipedia.org/wiki/Revenue_Act_of_1862

- Approved on July 1, 1862
- Sess 2, ch 119, 12 Stat 432, App July 1, 1862
- H.R. 312. House vote on April 8, 1862. https://www.govtrack.us/congress/votes/37-2/h157
- Passed with 73%

## Revenue Act of 1861

See http://en.wikipedia.org/wiki/Revenue_Act_of_1861

- Approved on August 5, 1861.
- Sess 1, ch 4, 12 Stat. 292, August 5, 1861 http://memory.loc.gov/cgi-bin/ampage?collId=llsl&fileName=012/llsl012.db&recNum=0323
- H.R. 54. Vote on August 2, 1861 https://www.govtrack.us/congress/votes/37-1/s98
- passed with 69%

## Appointments

### Secretary of Defense

- Edwin Stanton appointment on Jan 20, 1862

  - Nominated news Jan 14, 1862. "IMPORTANT FROM WASHINGTON. (1862, Jan 14). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91697554?accountid=13567"
  - Approval news Jan 16, 1862 "http://search.proquest.com/hnpnewyorktimesindex/docview/91681923/14278E6672B5FC7ED80/2?accountid=13567"


### Treasury Secretary

- Chase / William P. Fessenden:  July 1, 1863 (http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA202)
  By July 3, 1863, Market believes that Fessenden almost certain to be the secretary.
MONETARY AFFAIRS. (1864, Jul 04). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91861592?accountid=13567
- MuCulloch: March 8, 1865 (http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA202)


## Election of 1864

Election results are irrelevant. Well known that Lincoln would win after Atlanta.

### Democratic Convention

McClellan nominated in Democratic Party convention on Aug 29-31 in Chicago.

- McClellan Nomination news on Sept 1, 1864: "CHICAGO CONVENTION. (1864, Sep 01). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91811440?accountid=13567"


### Republican Convention


June 8-10, 1864.  The biggest headline was on June 9, 1864.

- NATIONAL UNION CONVENTION. (1864, Jun 08). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91756836?accountid=13567
- PRESIDENTIAL. (1864, Jun 09). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91857284?accountid=13567
- THE BALTIMORE NOMINATION. (1864, Jun 10). New York Times (1857-1922). Retrieved from http://search.proquest.com/docview/91818302?accountid=13567


## Election of 1862


States staggered, but many voted on Nov 4, 1862 (Mass & De on Nov 1, 1862). http://en.wikipedia.org/wiki/United_States_House_of_Representatives_elections,_1862.
A surprisingly large amount of news was reported on Nov 5, 1862.

- news of NY known on Nov 5, 1862 http://search.proquest.com/hnpnewyorktimesindex/docview/91696107/14278FACA51740C00C/31?accountid=13567
- news of illinois results on Nov 14, 1862. http://search.proquest.com/hnpnewyorktimesindex/docview/91646475/14278FACA51740C00C/23?accountid=13567

  
# Military Commanders

## Commanding General of the US Army

http://en.wikipedia.org/wiki/Commanding_General_of_the_United_States_Army

- Winfield Scott  -- Nov 1, 1861
- George McClellan Nov 1, 1861 -- March 11, 1862
- Henry Wagner Halleck: July 23, 1862 -- March 9, 1864
- Ulysses S. Grant: 9 March 1964 -- 9 March 1869

## Army of the Potomac

Due to its importance, it should be considered:

- Irvin McDowell: May 27--July 25, 1861
- George McClellan July 26, 1861 -- Nov 9, 1862
- Ambrose Burnside: Nov 9, 1862 -- Jan 26, 1863
- Joseph Hooker: Jan 26--June 28, 1863
- George G Meade: June 28, 1863--June 28, 1865  (Grant took operational control May 1864--April 1865)
    

# Foreign  Policy

## Trent Affair

- News of the Capture. Arrived in Ft. Monroe on Nov 15. Reported in newspapers on Nov 17.

    - THE GREAT REBELLION. (1861, Nov 17). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91597649?accountid=13567
	- not discussed in Monetary Affairs

- First news of British Response

    - Dec 16, 1861. MONETARY AFFAIRS. (1861, Dec 17). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91623771?accountid=13567

- Rumors and News of the release. Official response released on Dec 29, 1861

    - Dec 27, 1861. Private information.  MONETARY AFFAIRS. (1861, Dec 28). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91574570?accountid=13567
	- Dec 28, 1861. MONETARY AFFAIRS. (1861, Dec 29). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91540495?accountid=13567


## Lyon-Seward Treaty of 1862

Lyon-Seward Treaty of 1862 (proclaimed by Lincoln on July 7, 1862), signed by Lyons and Seward on April 7, 1862, ratified by the US on April 25, 1862.

Not discussed in financial pages.

## British Non Intervention News

July 30, 1862 : British non-intervention news


# Others

Events listed in prior sources, excluding battles

From McCandless

- Sherman's March, Nov 16-Dec 10, 1864
- Hood's Army Destroyed, Dec 15-16, 1864
- Savannah Falls, Dec 24, 1864
- Emancipation Proclamation, Sept 23, 1862
- Increase Greenbacks by 300 mill, Jan 8, 1862
- Re-election of Lincoln certain, Late Oct 1864
- Pasage of National Banking Act, Feb 25, 1863

From Smith and Smith, p. 706

- Jackson in Baltimore Rumor, Sept 3, 1862. (bad)
- French Troops in Mexico, May 3, 1864. (bad)
- Early near washington, Jul 11, 1864 (bad)
- British non-intervention news, Jul 30, 1862 (good)
- peace rumor, Feb 10, 1863 (good)
- peace rumor, Jan 19, 1865 (good)
- Richmond rumor, March 16, 1865 (good)
- British non-recognition, June 19, 1865 (good)
- 2nd legal tender act announced, Jun 12, 1862
- final vote on 2nd legal tender act, Jul 8, 1862
- WM recommends 3rd legal tender act, Jan 14, 1863
- House res on 3rd act, Jan 14, 1863
- army pay crisis, Jan 17, 1863
- House passes 3rd legal tender act, Jan 26, 1863
- Senate passes gold bill, Apr 16, 1863
- House passes gold bill, Jun 14, 1863
- Treasury gold sales approved, Mar 17, 1863
- Cooke bond sales success, Mar 25, 1863
- Chase gold sales, Apr 15-21, 1863
- Chase gold sales, May 20, 1863
- Foreign loan rumor, Oct 22, 1863
- successful loan issue, Sept 10, 1864
- McCulloch's nomination, Mar 8, 1865.

# Mitchell

From @Mitchell1903.

## Factors


The amount of debt. "The amount of the demand debt made made speedy repayment more doubtful.
Hense the effect of every suggestion of an increase in the amount of the paper currency was to decrease the value of the greenbacks already in circulation." (p. 199)

- Second Legal Tender Act:

	- Sec. Treas requested Congress to authorize 2nd issue of U.S. Notes. June 11, 1862: 96.22; June 12: 94.96. See NYT, June 12, 1862.
	- Fall continues to the final vote, the currency is 89.79

- Third Legal Tender Act:

	- Rumor that annual finance report would recomend anouther issue of US notes. Dec 1, 1862: 76.94; Dec 4, 1862: 74.63. Bankers, Vol XVII, p. 560.
	- Denial, reaction to 76.63.
	- Dec 8, Thaddeus Stevens intro a bill to issue \$200 mn, relapse to 75.19.
	- Dec 9, Thaddeus Stevents admits no chance of passage. *Congressional Globe*, 37 Cong. 3d Sess. pp. 23, 145. NYT, Dec 9, 1862.
	- Jan 8, Committee of Ways and Means submitted measure authorize increase of 300 mn. Fall to 72.99
	- Jan 14, House passes joint res for issue of 100 mn to pay army and navy.  Fall to 67.57.
	- Acquiescence of the Senate causes "slight further decline"
	- Ways and Means bill passed and sent to Senate, notes to 65.90 (Jan 26)
	- Three weeks later: Senate ammendment reduces new issue from 300 mn to 150 mn.
	- House refuses ammendment. Drop of 1.46.
	- Next two days, thought Senate would yield, decline of 60.98.
	- Feb 26, House yields and passes bill. Fell to 57.97.
	- Slight reaction to 58.48 on day bill became law. Mar 3, 1863.
	
- Financial condition of the Treasury department

	- "annual reports of the secretary of the treasury was narrowly watched by the gold market. For example, the annual reports of the secretary of the treasury were anxiously awaited each December and their appearance caused a rise or fall of the currency according as the condition of the finances presented seemed hopeful or gloomy.

	- 1862. Day before report circulates, rumor that another issue of greenbacks would be proposed. Currency fell 76.34 to 74.63, rose when report issued and recommends a national bank currency rather than govt notes. 
	- 1863: slight declien. Declared explicitly against an increase of greenbacks, estimated expenditures exceeded receipts, and obliged to ask for a loan of 900 mn.
	- 1864. fall, disappointing (editiorial *New York Tribune* Dec 8, 1864; money article Dec 7, 1864)
	- 1865. McCulloch recommends a speedy resumption of specie payments. Rise from 67.33 on the 5th to 68.14 on the 6th. (papers on Dec 6, *Hunt's Merchants' Magazine*, Vol LIV, p. 77)

- Ability of the government to borrow. "for the fate of a loan indicated public confidence or distrust, and success provided a means for continuing the war without the issue of more legal-tender notes."

	- Jay Cook's success in obtaining subscriptiosn for 5-20s. prior to Mar 23, 1863: 65, Mar 25: 71.68.
	- Report that foreign loan obtainted. Oct 21, 1863: 68.49; Oct 22: 70.05. Rumor Discredited, Oct 23: 68.26.
	- Sept 1864. Loan of of $32.5 million subscribed twice over with premium of 4 percent. Sept 9, 1864: 42.37; Sept 10: 45.87. (*Merchants' Magazine*, Vol LI, p. 292; *Rep of Sec Treas*, Dec 1864, p. 21)

- Changes in officials at the Treasury Department

	- Resignation of Assistant treasurer in NYC. June 1, 1864: 53.05, June 2, 1864: 52.63.
	- Jul 1, 1864: Sec Chase's resignation. Fall to 40. Few hours later, Fessender's appointment, reaction to 45.05.
	- McCulloch's nomination. March 7, 1865: 50.25; Mar 8: 51.05.

- Political changes. "Being of less frequence occurence, Political changes played a less prominent role in the gold market than financial and military affairs"

	- Dem party nominated Gen. McClellan on Aug 31. Fall from 42.73 to 41.15.
	- Penn State election in October "an indication of the probable outcome". First few days, uncertain who had won. October 11, *New York World* claims that the Dem won. Fall from 50.41 to 49.17.
	- "Curiously, the Republican triumph in November had the same effect upon the currency as this promise of Democratic success had exercised. I seems to have been argued that President Lincoln's re-election meant an indefinite prolongation of the war, and hence destroyed any chance of a speedy redemption of the paper money." Fall on 9th from 40.65 to 38.46; however a reaction quickly followed.
	- Lincolns assassination occurs after regular hours. Fall followed by quick reaction. Market closed for several days. A few days later back to the level it held before the assassination.

- Foreign relations. Overshadowed during the war by domestic affairs. They exercised some influence upon the falue of the paper money. Two matters of concern. Chance of foreign intervention and French occupation of Mexico.

	- Jul 30, 1862. Speech by Lord Palmerston, interpreted to  that Britich govt had no intent to intervence. Currency rose from 85.84 to 87.43. Dispatches to NY papers Jul 31, 1862 and editorials.
	- Feb 10, 1863. Report of Fra emperor about conf between North and South. Appreciation from 63.90 to 65.57. *NYT* money, Feb 10, 1863.
	- Jun 19, 1865. Widhdrawl of Brit recognition of Confed. Rise from 68.91 to 71.43. *NY Herald*, money, June 19, 1865.
	- May 3, 1864. Publication in *Le Moniteur* assuring stay of French troops in Mex. Depcreciate from 56.50 to 55.63. *NYT*, money article, May 3, 1864.
	- June 5, 1865: Napoleon had been urged to withdraw his troops. Decline from 73.94 to 73.13. *NY Herald*, money article, June 5, 1865.
	- Summer, complications on the Rio Grande. *Merchants' Magazine*, Vol LIII, p. 133.
	- Nov 7, 1865. Cabinet would notify France that sending further troops "would meet with the disapprobation of the United States." "Little fear of serious complication", *NY Herald*, money article, Nov 7, 1865.

- quantity of money. p 207-209
- speculation. p. 209-210. "there seems to be more danger of exaggerating than of minimizing its importance as an independent factor in the gold market."
	

## History of Price Movements

This is a summary of Part ..., Chapter III, Section III ([p. 210ff.](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA210))

1. January to April 1862 ([p. 212-213](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA212))

	- (-) 1st half of february slowness of Congress in framing tax bills 
	- (+) Grants capture of Fort Donelson
	- (+) 1st legal tender act
	- (+) Battle of Pea Ridge
	- (+) Monitor and merrimac
	- (+) McClellan advanced into Western Virginia
	- (+) Shield defeated Stonewall Jackson at Wincester on March 23.
	- End of February "confidence in the speedy end of the war was high"
	- In April, Adjutant general issued an order to stop recruiting
	- Stalemate at Shiloh
	- (+) Island No. 10 surrendered
	- (+) Fort Pulaski fell (news 15th)
	- (+) New Orleans falls

2. May 1862 to February 1863 ([p. 213-217](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA213))

	- May; mil ops turn versus north

	   - (-) Yorktown
	   - (+) Faragut took Natchez
	   - (-) Jacksons ops in the Shenandoah valley: Front Royal, Cross Keys, Port Republic

	- Fall of 2.90 in June

	   - (+) Hanover Courthouse, Seven Pines, and Fair Oaks slight rise
	   - (-) 12th Chase's request for 2nd issue of greenbacks announced
	   - (-) Stuart rode around McClellan

	- July

	   - (-) Peninsula ends in loss
	   - (-) President issue call of 300,000 volunteers
	   - (-) Morgan raiding in Kentucky
	   - (-) Congress passes 2nd legal tender act

	- August

	   - (+) Halleck  appointed general in chief
	   - (+) Report of House of Commons; British no intention of intervening
	   - (-) Lee forces Pope to Washingotn
	   - (-) Bragg northward movements in Kentucky
	   - (-) Sioux Indians commence outrages in Minnesota

	- September

	   - (-) Lee's advance cause fears of safety for Baltimore, Harrisburg, Philadelpha
	   - (-) Bragg and Kirby threaten Louisville and Cincinnati
	   - (+) South Mountain (14th)
	   - (+) Antietam (17th)
	   - (-) Jackson's capture of Harper's Ferry
	   - (-) McClellan failed to follow up; Lee crosses Potomac in safety
	   - (-) Rosecrans victory at Iuka

	- October

	   - (-) McClellan inactive
	   - (-) Stuart raid around McClellan's army
	   - (+) Rosecrans stops attempt to recapture Corinth
	   - (+) Brag retreats from Kentucky; Battle of Perryville
	   - (-) Elections: Maine, Michigan; Wisconsin

	- November

	   - (-) Order relieving McClellan published on Nov 10th, 1862

	- December

	   - (-) Chase report. Expenses increasing; no suggestion for increased taxation
	   - (-) Army in arrears
	   - (-) 3rd issue of US notes foreshadowed
	   - (-) Admin attacked for suspension of habeas corpus and emancipation
	   - (-) Sec state and treasury resign; Lincoln convinces them to stay
	   - (-) Burnside repulsed with a loss of 14,000 men

	- January / February

	   - (-) Framing of the 3rd legal tender act
	   - Hooker succeeded Burnside
	   - (-) Grant's first campaign against Vicksburg by destruction depot at Holly Springs and Sherman's repulse at Chickasaw Bayou.
	   - (-) Monitor foundered
	   - (+) Forrest's confed cavalry beaten at Parker's Cross Roads
	   - (-) Magruder recaptured Galveston
	   - (+) Sherman captured Arkansas post
	   - (-) Fed attack on Ft McAllister in Ga repulsed
	   - (+) Confed attack on Ft Donelson in Tenn repulsed
	   - dominating influence in the last half of the month was the 3rd legal tender act

	- Overview

	   - commenced with depreciation of 2 percent, ended with 42 percent
	   - May-Nov: dominating casuses were military disaster and 2nd issue of greenbacks
	   - jan-feb: legislation pending in congress.

3. Rise from March to August 1863 ([p. 217-220](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA217))

    - March

        - (-) no decisive mil ops: Grant at Vicksburg and Farragut at Port Hudson
	    - (+) Congress passed supplementary internal revenue act, nat banking act, and 900 million loan act.
	      According to Sec. Chase, enactment of laws followed "by an immediate revival of public credit.",
		  *Report of the Sec. of the Treasury*, Dec 1863, p. 2.

	- April

        - (+) Jay Cooke organization of 5-20 loan is well organized; receiving subscriptions
		- (-) unsuccessful bombardment of forts in Charleston
		- (+) Banks successful operations along Bayou Teche in Louisiana
		- (+) Grant moves forces South of Vicksburg and prepares to attack
		- (+) Hooker executed a well planned movement across the Rappahannock and Lee seemed at disadvantage

	- May
  
	    - (-) Batlte of Chancellorsville
		- (+) Grant's cavalry reaches Baton Rouge, main army forces Pemberton back into Vicksburg
		- (+) Banks closed in on Port Hudson

	- June

	    - "striking example of an appreciation of the currency --- small, to be sure --- despite military reverses". Ability of govt to borrow overwhelms the positive effect.
		- After Chancellorsville, Army of VA reinforced with conscriptions
		- (-) Lee at Winchester, captures 4,000 prisoners and proceeds up Shenandoah valley to Chambersburg in PA
		- Lincoln calls for 100,000 militia to repel invasion
		- Grant and Banks continue sieges, but repulsed and no significant gains
		- C.L. Vallandinham, who had been arrested for treasonous speach, nominated for Gov Ohio
		- New York "great Peace meeting"
		- U.S. receiving 1.5 to 2.5 million a day for government bonds.

	- July

        - Major military victories
		- Vicksburg
		- Port Hudson
		- Gettysburg
		- Sherman expels Johnson from Jackson
		- Confederates repulsed at Helena
		- capture of Morgan's raiders in ohio
		- draft riot in NYC (overshadowed)

	- August: continued good times

	    - military events were few: Grant, Meade and Lee resting troops
		- Rosecrans begins advance against Bragg at Chattanooga
		- Burnside moves on Knowxville
		- vigorous pushing of the siege in Charleston (*New York Times*, money articles, July 15, July 17, and Aug 25, 1863)
		- Draft completed in NYC
		- Elections in Ky, VT, and CA were favorable to the admin candidate.

4. Fall from Sept 1863 August 1864 ([p. 220-235](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA220))

	- Sept

	    - "Many expected after the great victories of July that the end of the rebellion was at hand"
		- (-) Disappointment that Lee escaped with army intact
		- (-) Chickamagua

	- Oct-Dec

	    - slow, steady depreciation
		- Lowest point 63.80 (Oct 15): rumors of another forward movement by Lee
		- Op in VA less important that op in TN
		- Grant relieves Rosecrans who had allowed himself to be trapped in Chattanooga
		- Lowest prices (Nov 21, 23) - due to investment of Burnsides forces in Knoxville
		- highest prices (Nov 27) - Grants victory at Chattanooga
		- (+) treasury report was "rather cherrful tenor", despite Chase asking to borrow an additional 900 million

    - Jan-March

        - During 1st 3 months of 1864, the slow decline continued.
		- Jan: armies "lay nearly still both east and west"
		- Feb 1st: draft ordered for 1.5 million men to serve 3 years or the war.
		- Gen. Butler sallied from Fort. Monroe in an attempt to take Richmond, but failed.
		- Kilpatrick's cavalry got through 1st and 2nd lines of defense to Richmond, but thwarted by the third.
		- Loss at Olustree
		- Sherman destroyed Meridian and cut line of railway from Mobile to the North
		- "hoped-for destruction of Pope's army not accomplished"
		- In March, failed Red River expedition starts (Banks and Taylor)
		- Forrest defeats W.S. Smith at Okalona, recaptures Jackson, repulsed at Paducah
		- "Most important in its effect upon the gold market", Congress slow to pass the finance bills.
		  *New York Tribune*: "A Congress fit to exist, would have matured and perfected *some* sort of finance
		  system before the close of its fourth month." (Editorial, March 29, 1864)

	- April

	    - "Congress still passed no revenue laws, and the war news was unfavorable"
		- (-) massacre of Fort Pillow
		- (-) failure of Red River expedition
		- (-) Steel retreats from Arkansas
		- (-) Confederates capture Ft. Williams
		- (-) Confederates capture Plymouth, N.C.
		- (+) slight successes in Texas
		- (+) Grant prepares for advance
		- (+) Union gains in elections in CT, RI, MO, NJ
		- "successes could not counterbalance these disasters and the inactivity of Congress"

	- May

	    - "In the first half of May there was a rise"
		- (+) Sherman sets out from Chattanooa, forces Johnson back from Dalton, Resaca, and Allatoona Pass
		- (+) Grant crossed Rapidan, Battles of Wilderness and Spottsylvania Court House.
	      "Every rumor from the field caused a rise or fall of the currency, but despite Grant's enormous
		  losses, he was believed to have the advantage, so that the general trend of the fluctuations was
		  upward until near the middle of the month." (*New York Times*, money article, May 12, 1864)
		- (-) 10th, Merrill's cavalry was defeated by Stuart
		- (-) 15th Siegl routed at Newmarket
		- (-) 16th Beauregard forced Butler back at Bermuda Hundred
		- (-) Lee forstalled Grant at North Anna
		- (-) Congress failed to pass tax bills
		- After 10th currency fell and by the 17th back to the opening value of the month back lower than in April

	- Gold Bills

	    - during 1st five months of 1864 depreciation of less than $2 month due to lack fo progress in rebellion and
		  slowness in voting tax bill
		- During June-July the depreciation accelerated in part due to the gold bill.
		- March 3, 1863 - stamp act upon time sales of gold. 0.5 percent of the moutn plus interest at 6 percent per annum, and forbade loans on the pledge of coin in excess of its par value. Followed by advance from 58.22 on 3 March to 66.67 on 6th, but by the 10th had fallen agin to 61.35
		- While the currency advanced March - Aug 1863, less concern about speculation.
		- Concern about speculation grew as the currency depreciated. Bills introduced and killed.
		- The govt was receiving excess gold from customs duties. Act passed that sold the excess gold. Currency appreciated from 59.61 on Mar 9 when it was expected to be defeated to 62.06 on 17th, when it passed.
		- importers allowed to deposit paper currency with subtreasury, and receive ceertifactes of deposite of gold at a rate slightly below the current premium. Announcement: 169.75 on 26th to 165.75 on the 29th of March.
		- This did not work, and the government followed the market rather than leading it. On April 13, the Chase stopped the sale of certificates after the 16th.
		- April 13th, Chase orders another gold sale. PRice falls from 177.25 on the 14th to 166.875 on the 21st. Price returns to original on the 2th.
		- Chase: "I see that gold is again going up. This is not unexpected. Military success is indispensalbe to its permanent decline, or, in the absence of military success, taation sufficient upon state bank issues and state bank credits to secure .... an exclusive national currency; and sufficient, also, to defay so large a proportion of current expenditures as to reduce the necessity for borrowing to the minimum." Letter of April 26, 1864, Warden, Life of Chase, p. 582.
		- Sold exchange upon London at a rate below market. Gold was 181.675 on May 19 to 181 on May 20th. Gold rose, and on the 24th the Treasury admitted defeat.
		- Bill to prohibt all contracts for sale of gold for future delivery and forbade sale of gold by a broker outside his own office. Sent by Chase to Senate Comm. on Finance and reported by shaerman on April 14th.
		- Gold bill passed by Senate on April 16th.
		- Gold bill delayed in House. Taken up on June 7th. A week later, a Very close vote. Required speaker's vote to secure consideration and passed 76 to 62. Signed by Lincoln on June 17.
		- Gold opened June at 190, rose to 197.5 on June 14.
		- Businesses petition government to repeal the act.
		- Chase resigns on June 29, Lincoln accepts the resignation on the 30th.
		- Governor David Tod of Ohio declines.
		- Repeal of act: introduced in the Senate on June 22; Jul 1 passed Senate and House with little debate; signed by Lincoln on Jul 2; on  Jul 5, the gold room reopens.

5. Rise from Aug 1864 to May 1865 ([p. 235-238](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA235))

	- Aug

	    - "slow but tolerably steady appreciation in the value of the currency, due to the improving military prospect"
		- (+) Sheridan takes command in Shenandoah - fear of capture of Washington removed
		- (+) Grant resumes offensive vs. Lee, seizes Weldon railway
		- (+) Sherman continues flanking movements vs. Atlanta
		- (+) Farragut captures Confed vessels in Mobile Bay
		- (-) "rate of advance, however, was checked by the condition of the finances". Unpaid requisitions reach 130 mn.

	- Sep

	    - (+) capture of Atlanta
		- (+) Sheridan defeats Early at Opequan and Fisher's Hill
		- (-) Meeting of Dem convention in Chicago at end of Aug - "reaction, but this was overcome by the good news from the front"

	- Oct

	    - (-) Kautz cavalry defeated on the 7th
		- (+) Hood's advance into Tennessee stopped at Allatoona Pass
		- (+) Sheridan defeats Early at Cedar Creek
		- (+) Albemarle sunk, cruiser "Florida" captured

	- Nov

	    - "curious reaction, due manly to the presidential election. Mr. Lincoln's triump was taken to mean an indefinite continuation of the war, and so depressed the value of currency"

		- most important military event: Sherman cuts connections with the North and marches to the Sea
		- Hood presses North
		- Schofield's withdrawl from Franklin "construed as a defeat"

	- Dec

	    - "interupted rise recommenced in Dec"
		- Sherman reached Savannah
		- Thomas defeats Hood at Nashville
		- (-) disappointing Finance Report
		- (-) bill introduced into House by Thaddeus Stevens: penalties for anyone pay more than face value for gold coin, or less than full value for paper money
		- (-) Butler fails to take Ft. Fisher

	- Jan

	    - (+) Terry captures Ft. Fisher
		- (+) Sherman prepares for march north from Savannah
		- (+) 23rd, attept to destroy Grant's shipping on the James failed

	- Feb

	    - (-) Grant's attempt to turn the Confederate lines at Hatcher's Run was unsucessful
		- (+) Sherman captures Charleston and Wilmington
		- (-) failure of "peace conference" at Hampton roads

	- March

	    - "very slow rate of appreciation became very rapid, because of the opening of the spring campaign"
		- March 14, news Sherman had reaced Laurel Hill, NC. Sherman had not been heard from since early Feb.
		- Sherman's victories at Averysboro and Bentonville
		- Sheridan joins Grant before Petersburg
		- Lee's position "desparate", made last assault on encompassing lines.
		- Grant beings advance
		- Lincoln's second inauguation
		- Fessenden replaced by McCulloch as Sec of Treas.

	- April

	    - victory at Five Forks
		- capture of Petersburg and Richmond
		- Lee's surrencer
		- Sherman takes Raleigh and Johnston surrenders
		- President Lincoln's dath was the "one untoward event"

	- May

	    - Further advance of above the average for April
		- Armed resitance ceased, Davis captured, war over
		- Success in selling 7.30 notes to pay the army

6. Decline from June to Dec 1865 ([p. 238](http://books.google.com/books?id=ElAuAAAAYAAJ&pg=PA238))

	- gold market calm "Great fluctuations gave place to slight variations from day to day"
	- May 11 was 77.82 followed by a slow decline
	- 7.30 notes well received, but sill increased debt.
	- danger of war with France because of Maximilian in Mex.
	- In Dec, slight reaction to McCulloch finance report recommending a speedy resumption of specie payments and indorsement of this policy by the House of Rep.

# Events

## French troops in Mexico

- "The later foreign news was construed this afteronnon as less favorable for Exchange and Gold." MONETARY AFFAIRS. (1864, May 04). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91841251?accountid=13567
- FRANCE. (1864, May 03). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91822739?accountid=13567

```yaml
"French troops in Mexico":
  start_date: 1864-5-3
  good: false
```

# British non-intervention news

- MONETARY AFFAIRS. (1862, Jul 31). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91713952?accountid=13567
- MONETARY AFFAIRS. (1862, Jul 30). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91652587?accountid=13567
- ENGLAND AND AMERICA. (1862, Jul 29). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91705546?accountid=13567

```yaml
"British non-intervention news"
  start_date: 1862-7-29
  end_date: 1862-7-30
```

# Adjutant Halts Recruiting in Feb 1862

Mitchell cites order by Adjutant general to stop recruiting; could not find relevant newspaper articles.

# Hampton Roads Conference

Discussions occur on the 3rd; Rumors about the results appear in the newspapers on the 5th.
Official statements from Jefferson on the 10th, and Lincoln on the 11th.
However, this is exactly the sort of event for which markets would have insider information due to access to politicians.

```yaml
"Hampton Roads Conference":
  start_date: 1862-2-3
  end_date: 1862-2-5
  good: false
```

- THE PEACE CONFERENCE. (1865, Feb 03). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91910265?accountid=13567
- The peace conference. (1865, Feb 04). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91921630?accountid=13567
- FROM, W. (1865, Feb 05). THE PEACE CONFERENCE. New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91968625?accountid=13567
- THE PEACE CONFERENCE. (1865, Feb 06). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91958383?accountid=13567\
- MONETARY AFFAIRS. (1865, Feb 03). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91904452?accountid=13567
- MONETARY AFFAIRS. (1865, Feb 04). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91912145?accountid=13567Q
- MONETARY AFFAIRS. (1865, Feb 06). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91952554?accountid=13567
- MONETARY AFFAIRS. (1865, Feb 07). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91973595?accountid=13567 Does not mention peace conference

" ... with the certainty that the war is now to be carried on with renewed vigor and determination on both sides ..." (NYT, Feb 6).

# 1st Legal Tender Act (Act of Feb 25, 1862)

(12 Stat 345)


## Lincoln's election

Election on Nov 6, 1860.
The next day it was seen as probably that Lincoln won  (Nov 7, 1860):

    Astounding triumph of republicanism. (1860, Nov 07). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91574664?accountid=13567

And by Nov 8, 1860, it was almost certain that Lincoln had won:

    THE ELECTION. (1860, Nov 08). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91581878?accountid=13567

## New York Times Chronological Tables

Each year of the war the New York Times produced a chronological summary of the events of each year

- HISTORY OF THE REBELLION. (1861, Dec 31). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91547816?accountid=13567
- A YEAR OF WAR. (1862, Dec 31). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91693289?accountid=13567
- BATTLE RECORD FOR 1863. (1863, Dec 31). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91811410?accountid=13567
- 1864. (1864, Dec 31). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91862813?accountid=13567
- EVENTS IN 1865. (1865, Dec 30). New York Times (1857-1922) Retrieved from http://search.proquest.com/docview/91969412?accountid=13567
