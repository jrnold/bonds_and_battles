# Treasury Reports

1863, p. 11--12. Makes assumptions about what the expenditures woud be in peace. Discusses what the peace expenditures would have been in 1862, 1863, and 1864.

- Army and Navy peace expenditures are double those of the last year of peace. $55,843,834.48.
- Pay Civil Service, Pensions, and Indians at the same level.
- Normal receipts pay for ordinary expenditures and interest on the debt. Loans pay for 
  for war expenses.

On the effect of the war on debt (p. 12-13)

    These statements illustrate equally the importance of an economical and
    vigorous prosecution of the war. No prudent man will recommend economy at
    the expense of efficiency. Such nominal economy is real extravagance. But
    efficiency is not promoted by profusion, or waste, and least of all by misuse of
    public money or public property. " Every dollar and every man are freely offered /
    by a generous people. How sacred the obligation that not one man should be
    wasted, and not one dollar misapplied. Nor is rashness, in war, vigor. But
    the vigilance that misses no opportunity, the energy that relaxes no effort, the
    skill that utilizes all resources, and the perseverance that never grows weary— 
    these make true vigor. If by such vigor the rebellion can be suppressed and
    the war ended before the 1st of July next, the country will be saved from the
    vast increase of debt which must necessarily attend its continuance during
    another year, and the debt itself can at once be placed in a course of steady reduction.
    And whenever progressive payment shall begin, the value of national
    securities will rapidly rise, and reduction in rates of interest will gradually
    diminish the burdens of debt. 

Discussing the debt (p. 13)

    Towards the accomplishment of the first object [moderate interest], the nearest approach that
    seems possible has been made. The earliest negotiations were at the highest
    rates of interest; for it is a distinguishing characteristic of our financial history in
    this rebellion that the public credit, which was at the lowest ebb in the months
    which preceded its breaking out, has steadily improved in the midst of the terrible
    trials it has brought upon the country. The first loans were negotiated at seven
    and thirty hundredths per cent; the next at seven; the next at six; more recently
    large sums have been obtained at five and four; and the whole of the
    debt which is represented by. United States notes and fractional currency bears,
    of course, no interest.

and later (p. 13)

    It will not escape observation that the average rate is now increasing,
    and it is obvious that it must continue to increase with the increase of the proportion
    of the interest-bearing to the non-interest-bearing debt. And as the
    amount of the latter, consisting of United States notes and fractional currency,
    cannot be materially augmented without evil consequences of the roost serious
    character, the rate of interest must increase with the debt, and approach continually
    the highest average. That must be greater or less in proportion to the
    duration and cost of the war. 

# Repayment of Debt Estimates

## Treasury Report of 1865

Calculates the years until repayment

- Assumes a payment of $200 per year, and that the debt is funded at 5 percent and 5.5 percent.
- Assumes that on July 1, 1866 ("when all our liabilities shall be accurately ascertained"), the debt will be $3,000,000,000.
- The annual interest on $3 billion is 165 million at 5.5 percent and 150 million at 5 percent.
- The 200 million is applied at half yearly installments of 100 million each.
- The debt would be paid in 32 1/8 years at 5.5 percent, and a little over 28 years at 5 percent.
- Notes that the assumed interest rates are too low since the current debt is funded at an average higher than 5 percent. Suppose that the government cannot refund its debt at that rate until 1869. The debt will have decreased by 100 million in 3 years. Then the debt will be paid off in 27 years at 5.5 per cent and something less than 27 years at 5 percent.

Using the number of payments to repay an annuity, I get numbers similar to that
$$
n = -\ln(1 - \frac{r * A}{P}) / \ln(1 + r)
$$
where $r$ is the interest rate, $A$ is the amount, and $P$ is the payment amount.


```{r}
debt <- 3e9
r0 <- 0.06
r1 <- 0.055
r2 <- 0.05
payment <- 2e8

annuity_number_payments <- function(x, r, p) {
  -log(1 - (x * r / p)) / log(1 + r)
}
annuity_number_payments(debt, r1 / 2, payment / 2) / 2
annuity_number_payments(debt, r2 / 2, payment / 2) / 2

```

```{r}
annuity_number_payments(3e9, r1, payment) + 1
annuity_number_payments(3e9 + 500e6, r2, payment) + 2
annuity_number_payments(3e9 + 2 * 500e6, r2, payment) + 3

```

Calculates the burden of the debt relative to the national resources (p. 24-25)

- The "products of agriculture, mining, mechanic arts, commerce, fisherie, and forests" in 185 were 28.9 percent of the value of real personal property. Assume that that value of production is 25 percent of personal property.
- In 1860 real an personal propety of the Union (slaves excluded) was 14,183 million.  Of this, the Confederate states had 3,467 million with an increase of 139.7 percent. Of this, the Union states had 10,716 million with an increase of 126.6 percent over 1850. On average, the increase was 129.7 percent. 
- Suppose Union states have same growth rate for captial 24,111 million in 1870.
- Suppose Confederate states had no increase in 1870.
- Total capital in 1870 is 27,578 million.

# Analysis of the Repayment of the Public Debt

- @Elder1863
- @Elder1865
- Annual Report of the Treasury 1863
- Annual Report of the Treasury 1865
- @IAH1864a
- @walker1865wealth
- @Gibbons1867
