# Available data and prices

- Greenbacks (Union currency): This has the highest frequency (daily), and likely the highest liquidity of all the assets considered, and thus the most sensitivity to events.
  However, it is only available from January 1, 1862, and thus misses all events at the start of the war.

- Union bonds: Use either one or severa of either the interest rates or prices of Union bonds, e.g. 5's of 1874, 6's of 1868, 6's of 1881. This has relatively high frequency, approximately weekly.
  Unlike the greenbacks, these series or a series constructed from them can span the entire war.

- Slope of the yield curve: This may be related to the duration or severity of the war. The more negative it is the more risky things are in the short term.
  This would 

- Difference between the yields of Northern (either state or U.S. government) and Southern bonds**.
  Since some Confederate state bonds (Virginia, NC, Georgia, Tenn) are traded in NYC, this does not have the timing issue

- Yield or price difference between the same bond sold in Richmond and NYC** There is some price data on NC, Virginia, and even Tenn bonds sold in NYC and Richmond.
  The difference in yields between the same bond sold in different markets could either indicate differences of beliefs, or differences in the risk to investors.
  I find the later more believable than the former; this could occur if the Confederacy wins and pays southern investors but defaults on Northern investors.
  This seems possible, provided that the markets were sufficiently non-integrated, which seems possible since the Southern states selectively defaulted on Northern creditors at the start of the war.
  Since bonds in both markets would have the same risk due to the cost of the war, the differential should be only due to the probability of victory.

- Yield or price difference between different types of Southern bonds.
  Davis and Peque makes the argument that Confederate bonds had the most risk if the Confederacy lost, followed by states, followed by cities. This could also hold for North Carolina (or any other state) which had bonds issued during the war (which were defaulted upon) and those issued before the war.


# Handling News Latency

Different markets (NYC, Richmond) receive news at different times from different places.
How to model this?

1. Lag X and Lag the factors. This allows for events to affect the factors at different times, and the factors to affect the prices in different markets at different times. Constraints may include setting the lag to 1 for NYC and letting richmond be slower.
2. Have different factors by theater. Constrain lags and coefficients to be the same for theater and factor.
3. Model this all with implicit factors resulting imposed via priors

