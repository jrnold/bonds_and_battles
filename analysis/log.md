---
title: log
---


# 2015-07-15

- In Stan models, comparing t-distributions in the innovations of the latent factor.
  When the degrees of freedom is treated as a parameter, the posterior distribution of it and the scale are highly correlated,
  creating low effective sample sizes. The posterior for `nu` also appears to be the same as the prior.
- Given the computational issues of estimating df in the t-distribution, I'll estimate it with a normal distribution and a t-distribution
  with low df (e.g. 4-6) as a robustness check.

# 2015-07-19

- Figure out the set of models to run.
- 
