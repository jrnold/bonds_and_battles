=====================
Debt Acts
=====================

Loan of July and August 1861, Old Demand Notes, Seven Thirties
---------------------------------------------------------------

On `De Knight, p. 81 <http://books.google.com/books?id=0cQmAQAAMAAJ&vq=seven%20thirty&pg=PA81>`__,
`Bayley, p. 78 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA78#>`__.

Bill introduced into the House on July 9, considered in committee the whole oftht next day.
Passed the House 150 yeas, 5 nays.
Passed Senate without much debate.
Approved July 17, 1861 (12 Stat 259).
Authorizes 250 mn in notes or bonds. Of which 50 mn could be non-interest bearing demand notes.

Bill supplemental to the act introuduced in Senate on July 22, passed both houses with little debate.
Approved on Aug 5, 1861 (12 Stat 313).
Authorized issue of bonds at 6 percent, exchangeable for treasury notes bearing interest at 7.30 percent.

Act of Feb 12, 1862 (12  Stat 338) authorized the issue of 10 million additional demand notes.

Five Twenties of 1862
---------------------------------

Issued under the act of Feb 25, 1862 (12 Stat 345).
Authorized up 500 mn in these loans.

Act of March 3, 1864 (13 Stat 13) authorized an additional issue of 11 mn for those who had already subscribed to the loans issued under the former act.

Temporary Loan
----------------------------------

`Bayley, p. 79 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA79>`__

- Act of Feb 25, 1862 (12 Stat 345) allows issue of temporary loans on deposit for not less than 30 days in sums not less than 100 dollar, paying 5 percent per annum. May be withdrawn after 10 days. Authorizes 25 mn.
- Act of March 17, 1862 (12 Stat 370) increases the authorization to 50 mn.
- Act of June 11, 1862 (12 Stat 532) increases the authorizeation to 100 mn. 50 mn of the legal tenders authorized by the act reserved for that purpose.
- Act of June 30, 1864 (13 Stat 219) authorizes a further increase to 150 mn, and increases the interest to 6 percent.
- Total received was 716,099,247.16.

Legal Tender Notes
---------------------------------

`Bayley, p. 80 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA80>`__

- Act of Feb 25, 1862 (12 Stat 345). Authorizes the issue of treasury notes to the amount of 150 mn, bearing not interest,
  of such denominations as expedient, but not less than $5.
  50 million of this was in lieu of the the deman notes issued under the act of July 17, 1861.
  It was however, exchangeable for fonds of the US bearing 6 per cent interest.

  - Bill introduced to the House on Jan 22, 1862

- Act of July 11, 1862 (12 Stat 532) authorized an **additional** issue of 150 mn, at any denominations, but no fractional dollars, and not more than 35 mn in denominations less than $5.
  - Act of Mar 3, 1863 (12 Stat 710) authorized an additional issue of 150 mn, including the amount authorized in the Res of Jan 17, 1863 (12 Stat 822) of such denominations not less than $1.

Certificates of Indedebtedness
-------------------------------

`Bayley, p. 81 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA81>`__

Issue of certificates bearing 6 percent

- Introduced to senate on Feb 27, 1863 and passed both houses without debate.
- Approved on March 1, 1862 (12 Stat 352)
- Act of March 17, 1862 (12 Stat 370) authorized the issue of cerficiates to public creditors in payment of officer's checks.
- Act of March 3, 1863 (12 Stat 710) made the interest payable in lawful currency.
- 561,753,241.65 were issued  

Fractional Currency
---------------------------

`Bayley, p. 82 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA82>`__

- Bill passed both houses without debat, signed by the President the same day. Act approved July 17, 1862 (12 Stat 592).
- Terms altered in the acts of March 3, 163 (12 Stat 711) and June 30, 1864 (13 Stat 220).

Loan of 1863, One and Two Year Notes of 1863, Compound Interest Notes, Coin Certificates
-----------------------------------------------------------------------------------------

`Bayley, p. 82 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA82>`__

- bill introduced  on Dec 8
- considered in committee on Dec 23, 1862. **Controversial** because it proposed 200 mn in legal tender notes,
  1 billion in 6 percent, and taxing state banks.  
- Substitute reported from the committee on ways and means in the House on Jan 8, 1863, first considered in committee
  on Jan 12, 1863.  Proposed 900 mn in 6 percent bonds, 300 mn in treasury notes at 5.475 per cent, 50 mn in fractional
  currency, and a tax of 2 percent on state banks.
- Passed and became law on March 3, 1863 (12 Stat 709)
- Authorizes the issue of
    
- 300 mn this year, 600 mn next year for coupon bonds, not less than 10 or 40 year from date, in coin,
  denomination not less than $50, interest rate not exceeding 6 percent. "There shall be outstanding of bonds,
treasury ntoes, and United States notes, at any time, issued under the provisions of this act, no greater amount altogether than the sum $900,000,00."
- bonds of the amount $75 mn issued with interest at 6 percent. Act of June 30, 1864 (13 Stat 219, sec 3) limited the issues to $75 mn.
- Second section authorizes the issue of $400 mn in treasury notes, payable not exceeding 3 years, interest not exceeding 6 percent, paid in lawful currency. Made legal tender. Authorizes 150 million in US notes for such exchanges, but they cannot be used for any other purposes than that to exchange for these treasury notes.  Treasury notes issued amounting to 44,520,00 redeemable 1 year from date and $166,480,000 redeemable 2 years from date.  These notes bore 5 percent interest.
- Second section of the Act of of Mar 3, 1863 (12 Stat 710) authorizes the issue of compound interest notes. The amount issued was $17,993,760.
  The act of June 30, 1864 (13 Stat 218) authorize the issue of $200 mn in treasury notes of any denomination not less than $10, payable not exceeding 3 years, and interest not exceeding 7.30 percent, payable in lawful money, and legal tender.
- Section 5 of the act of Mar 3, 1863 authorizes the issue of coin certificates. For the amount of $981,134,880.46 issued.
  
Ten-Forties of 1864, Five-Twenties of March, 1864
-----------------------------------------------------

`Bayley, p. 84 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA84>`__

- Feb 25, 1864 submitted an ammendment to the act of Mar 3, 1863.
- On Feb 29, 1864 the act passed House without further debate.
- Mar 3, 1864 passed both houses and approved by the presiden
- It approved
    
 - Issue of 5 percent bonds of 196,118,300 issued, redeemable after 10 and payable after 40 years
 - Issue of 6 percent bonds of 3,882,500 issued, redeemable after 5 and payable after 20 years

 Five Twenties of June, 1864; Seven Thirties of 1864 and 1865
 ----------------------------------------------------------------

 `Bayley, p. 85 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA85>`__

- A bill to authorize a loan of $400 mn was reported in the House June 20, 1864.
- Provision that all bonds, treasury notes, and obligations of the U.S. exempt from taxes by state or municipal authority.
  That provision was **highly controversial**.
- Act approved June 30, 1864 (13 Stat 218)
- Sec 1 authorized the borrowing of $400 mn and issue coupon or registered bonds, redeemable in 5-30 years at the pleasure of the government, and payable not more than 40 years, interest not to exceed 6 percent, payable in coin.
  All bonds, treasury notes, or other obligations of the US to be excempt from state or municipal authority.
- 5-20 bondes to the amount of $125,561,300 issued.
- Section 2 authorized the issue in lieu of equal amount of bonds authorized in the same loan, $200 mn in treasury notes of  any denomination greater than $10, payable not exceeding 3 years, interest not exceeding 7.30, payable in lawful currency.
  The total amount of bonds and notes issued under this act not to exceed $400 mn and the total amount of US notes issued was not to exceed that sum, plus another $50 mn to redeem the temporary loan. 
  Notes issued under this act were not legal tender for repaying banks.
- Sec 3 authorized the Sec Treas to pay interest semi-annually on bonds previously issued, and lieu of bonds previously authorized with interest payable annually, otheres may be issued with interest semi-annually. Authorized to exchange treasury notes bearing interst at 7.30 percnet like bonds of all denominations in which such loans had been issued (not sure what that means).
- Act of Mar 5, 1865 (13 Stat 468) authorized further loan of $600 mn and issue of bonds of treasury loans.


Five Twenties of 1865--Consols of 1865, 1867, and 1868
--------------------------------------------------------

 `Bayley, p. 89 <http://books.google.com/books?id=Ce4JAAAAIAAJ&pg=PA85>`__

- bill reported to committee on Feb 27, 1865. Take up for consideration in  committee of the whole immedately.
- Authorized borrowing $600 mn for the next FY, upon similar terms as previous loans, but no authorization for legal tender or denominations smaller than $50.
  Annual interest limited to 6 percent in coin, or 7.30 in currency, redeemable in not less than 5 or more than 40 year.
- **limited debate** on the bill. Unsuccessful effor to fix the interest rate at 8 percent currency, and no further issue of treasury notes.
- Bill became law March 3, 1865 (13 Stat 468).
    

