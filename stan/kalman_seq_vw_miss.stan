functions {
  matrix make_symmetric_matrix(matrix x) {
    return 0.5 * (x + x');
  }

  // Convert vector to matrix of size
  // assumes that vector is in col-major order
  matrix to_matrix_wdim(vector v, int r, int c) {
    matrix[r, c] res;
    for (j in 1:c) {
      for (i in 1:r) {
        res[i, j] <- v[(j - 1) * (r * c) + i];
      }
    }
    return res;
  }

  /*
  Kalman Filter

  - int n: number of observations
  - int r: number of variables
  - int p: number of states
  - vector[n] y: observations
  - int miss[n]: 1 if y is missing, 0 otherwise
  - vector[n] b: See definition
  - vector[p] F[n]
  - vector[p] V[n]
  - vector[p] g[n]
  - vector[n]

  Returns

  `vector[2 p^2 + 2 p + 2] ret[n + 1]`: Each element in the array is a time period.
   The first element is $t = 0$ for the initial values of $m$ and $C$, the 2nd element
   is $t = 1$ and so on. Each vector is $[m_t, C_t, a_t, R_t, e, Q^{-1}]$,
   where matrices are converted to vecctors as column-major using `to_vector`.
   The indices of these elements are:

   - $[1, p]$: $m_t$
   - $[p + 1, p + p ^ 2]$: $C_t$
   - $[p + p^2 + 1, 2 p + p^2]$: $a_t$
   - $[2p + p^2 + 1, 2 p + 2p^2]$: $R_t$
   - $2p + 2p^2 + 1$: $f_t$
   - $2p + 2p^2 + 2$: $Q^{-1}_t$

  Each vector is $2 p^2 + 2 p + 2$ in

  */

  vector kfs_mat2vec(int r, int p,
                      vector m, matrix C,
                      vector a, matrix R,
                      vector e, vector Q_inv) {
    vector[2 * p + 2 * p * p + 2 * r] res;
  }

  vector[] kfs_filter(int n, int r, int p,
                        vector[] y, int[,] miss,
                        vector[] b, matrix[] F, vector[] V,
                        vector[] g, matrix[] G, matrix[] W,
                        vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    vector[r] Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    vector[r] e;
    // Kalman gain
    vector[p] K;
    // returned data
    vector[2 * p + 2 * p * p + 2 * r] res[n + 1];
    vector[p * p] C_vec;
    vector[p * p] R_vec;

    m <- m0;
    C <- C0;
    res[1] <- rep_vector(0.0, 2 * p + 2 * p * p + 2 * r);
    for(i in 1:p) {
      res[1, i] <- m[i];
    }
    C_vec <- to_vector(C);
    for (i in 1:(p * p)) {
      res[1, p + i] <- C_vec[i];
    }
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- quad_form(C, G[t] ') + W[t];
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          e[j] <- 0.0;
          Q_inv[j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv[j] <- 1.0 / Q;
          // forecast error
          e[j] <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv[j];
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e[j];
          C <- make_symmetric_matrix(C - K * Q * K ');
        }
      }
      for(i in 1:p) {
        res[t + 1, i] <- m[i];
      }
      C_vec <- to_vector(C);
      for (i in 1:(p * p)) {
        res[t + 1, p + i] <- C_vec[i];
      }
      for(i in 1:p) {
        res[t + 1, p + p * p + i] <- a[i];
      }
      R_vec <- to_vector(R);
      for (i in 1:(p * p)) {
        res[t + 1, 2 * p + p * p + i] <- R_vec[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + i] <- e[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + r + i] <- Q_inv[i];
      }
    }
    return res;
  }

  vector[] kfs_loglik(int n, int p, int r,
                        vector[] y, int[,] miss,
                        vector[] b, matrix[] F, vector[] V,
                        vector[] g, matrix[] G, matrix[] W,
                        vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    real Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    real e;
    // Kalman gain
    vector[p] K;
    // Loglik
    vector[r] loglik[n];

    m <- m0;
    C <- C0;
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- quad_form(C, G[t] ') + W[t];
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          loglik[t, j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv <- 1.0 / Q;
          // forecast error
          e <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv;
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e;
          C <- make_symmetric_matrix(C - K * Q * K ');
          loglik[t, j] <- - 0.5 * (log(Q) + e ^ 2 * Q_inv);
        }
      }
    }
    return loglik;
  }

  vector kfs_get_m(int t, int p, vector[] v) {
    vector[p] x;
    x <- segment(v[t + 1], 1, p);
    return x;
  }
  vector[] kfs_get_m_all(int n, int p, vector[] v) {
    vector[p] res[n + 1];
    for (t in 0:n) {
      res[t + 1] <- kfs_get_m(t, p, v);
    }
    return res;
  }
  matrix kfs_get_C(int t, int p, vector[] v) {
    vector[p * p] x;
    matrix[p, p] res;
    x <- segment(v[t + 1], p + 1, p * p);
    res <- to_matrix_wdim(x, p, p);
    return res;
  }
  matrix[] kfs_get_C_all(int n, int p, vector[] v) {
    matrix[p, p] res[n + 1];
    for (t in 0:n) {
      res[t + 1] <- kfs_get_C(t, p, v);
    }
    return res;
  }
  vector kfs_get_a(int t, int p, vector[] v) {
    vector[p] res;
    res <- segment(v[t + 1], p + p * p + 1, p);
    return res;
  }
  vector[] kfs_get_a_all(int n, int p, vector[] v) {
    vector[p] res[n];
    for (t in 1:n) {
      res[t] <- kfs_get_a(t, p, v);
    }
    return res;
  }
  matrix kfs_get_R(int t, int p, vector[] v) {
    vector[p * p] x;
    matrix[p, p] res;
    x <- segment(v[t + 1], 2 * p + p * p + 1, p * p);
    res <- to_matrix_wdim(x, p, p);
    return res;
  }
  matrix[] kfs_get_R_all(int n, int p, vector[] v) {
    matrix[p, p] res[n];
    for (t in 1:n) {
      res[t] <- kfs_get_R(t, p, v);
    }
    return res;
  }
  vector kfs_get_e(int t, int p, int r, vector[] v) {
    vector[r] res;
    res <- segment(v[t + 1], 2 * p + 2 * p * p + 1, r);
    return res;
  }
  vector[] kfs_get_e_all(int n, int p, int r, vector[] v) {
    vector[r] res[n];
    for (t in 1:n) {
      res[t] <- kfs_get_e(t, p, r, v);
    }
    return res;
  }
  vector kfs_get_Q_inv(int t, int p, int r, vector[] v) {
    vector[r] res;
    res <- segment(v[t + 1], 2 * p + 2 * p * p + r + 1, r);
    return res;
  }
  vector[] kfs_get_Q_inv_all(int n, int p, int r, vector[] v) {
    vector[r] res[n];
    for (t in 1:n) {
      res[t] <- kfs_get_Q_inv(t, p, r, v);
    }
    return res;
  }

  vector[] kfs_filter_loglik(int n, int p, int r, vector[] v, int[,] miss) {
    vector[r] ll[n];
    vector[r] err;
    vector[r] Q_inv;
    for (i in 1:n) {
      err <- kfs_get_e(i, p, r, v);
      Q_inv <- kfs_get_Q_inv(i, p, r, v);
      for (j in 1:r) {
        if (int_step(miss[i, j])) {
          ll[i, j] <- 0.0;
        } else {
          ll[i, j] <-  - 0.5 * (- log(Q_inv[j]) + (err[j] ^ 2) * Q_inv[j]);
        }
      }
    }
    return ll;
  }

  /*
  Backward sampling given results of a Kalman Filter

  - int n: Number of observations
  - int p: Number of states
  - matrix[p, p] G[n]: Transition matrices
  - vector[p] m[n + 1]: Mean of filtered states $p(\theta_t | y_1, \dots, y_t)$.
    Note that there are $n + 1$ entries, because $i = 1$ corresponds to the initial
    values at $t = 0$.
  - matrix[p, p] C[n + 1]: Covariance of filtered states $Cov(\theta_t | y_1, \dots, y_{t - 1})$.
  - vector[p] a[n]: Mean of predicted states $p(\theta_t \ y_1, \dots, y_t)$.
  - matrix[p, p] R[n]: Covariance of predicted states $Cov(\theta_t | y_t, \dots, y_{t - 1})$.

  */
  vector[] kfs_backward_sampler_rng(int n, int p,
                                    matrix[] G,
                                    vector[] m, matrix[] C,
                                    vector[] a, matrix[] R) {
    vector[p] theta[n + 1];
    vector[p] h;
    matrix[p, p] H;
    matrix[p, p] R_inv;
    // TODO: check dim;

    // set last state
    theta[n + 1] <- multi_normal_rng(m[n + 1], C[n + 1]);
    for (i in 0:(n - 1)) {
       int t;
       t <- n - i;
       R_inv <- inverse(R[t]);
       h <- m[t] + C[t] * G[t] ' * R_inv  * (theta[t + 1] - a[t]);
       H <- make_symmetric_matrix(C[t] - C[t] * quad_form(R_inv, G[t]) * C[t]);
       theta[t] <- multi_normal_rng(h, H);
    }
    return theta;
  }



}
data {
  // dimensions
  int n; // number of observations
  int p; // number of states
  // observations
  vector[n] y;
  int y_miss[n];
  // system matrices
  // observation equation
  vector[p] F;
  real b;
  // system equation
  matrix[p, p] G;
  vector[p] g;
  // initial conditions
  vector[p] m0;
  cov_matrix[p] C0;
}
parameters {
  real<lower = 0> V;
  cov_matrix[p] W;
}
transformed parameters {
  // log-likelihood
  vector[n] loglik;

  // Variables to save
  // prior of state: p(theta_t | y_1, ..., y_{t-1})
  vector[p] a[n];
  matrix[p, p] R[n];
  // likelihood of obs: p(y | y_t, ..., y_t-1)
  vector[n] err;
  vector[n] Q_inv;
  // posterior of states: p(theta_t | y_1, ..., y_t)
  vector[p] m[n + 1];
  matrix[p, p] C[n + 1];

  vector[2 * p + 2 * p * p + 2] kfs_res[n + 1];
  {
    matrix[p, p] G_tv[n];
    vector[p] g_tv[n];
    matrix[p, p] W_tv[n];
    vector[n] b_tv;
    vector[p] F_tv[n];
    vector[n] V_tv;
    //vector[2 * p + 2 * p * p + 2] kfs_res[n + 1];
    G_tv <- rep_array(G, n);
    g_tv <- rep_array(g, n);
    W_tv <- rep_array(W, n);
    F_tv <- rep_array(F, n);
    V_tv <- rep_vector(V, n);
    b_tv <- rep_vector(b, n);

    kfs_res <- kfs_filter(n, p, y, y_miss, b_tv, F_tv, V_tv, g_tv, G_tv, W_tv, m0, C0);
    a <- kfs_get_a_all(n, p, kfs_res);
    R <- kfs_get_R_all(n, p, kfs_res);
    m <- kfs_get_m_all(n, p, kfs_res);
    C <- kfs_get_C_all(n, p, kfs_res);
    err <- kfs_get_e_all(n, p, kfs_res);
    Q_inv <- kfs_get_Q_inv_all(n, p, kfs_res);
    loglik <- kfs_filter_loglik(n, p, kfs_res, y_miss);
    //loglik <- kfs_loglik(n, p, y, y_miss, b_tv, F_tv, V_tv, g_tv, G_tv, W_tv, m0, C0);
  }
}
model {
  increment_log_prob(sum(loglik));
}
generated quantities {
  vector[p] theta[n + 1];
  {
    matrix[p, p] GG[n];
    GG <- rep_array(G, n);
    theta <- kfs_backward_sampler_rng(n, p, GG, m, C, a, R);
  }
}
