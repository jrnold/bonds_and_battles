// -*- mode: stan -*-
functions {
  // --- BEGIN: dlm ---
  matrix make_symmetric_matrix(matrix x) {
    return 0.5 * (x + x');
  }

  int dlm_filter_return_size(int r, int p) {
    int ret;
    ret <- 2 * p * p + 2 * p + 2 * r;
    return ret;
  }

  // Convert vector to matrix of size
  // assumes that vector is in col-major order
  matrix to_matrix_wdim(vector v, int r, int c) {
    matrix[r, c] res;
    for (j in 1:c) {
      for (i in 1:r) {
        res[i, j] <- v[(j - 1) * (r * c) + i];
      }
    }
    return res;
  }

  /*
  Kalman Filter

  - int n: number of observations
  - int r: number of variables
  - int p: number of states
  - vector[n] y: observations
  - int miss[n]: 1 if y is missing, 0 otherwise
  - vector[n] b: See definition
  - vector[p] F[n]
  - vector[p] V[n]
  - vector[p] g[n]
  - vector[n]

  Returns

  `vector[2 p^2 + 2 p + 2] ret[n + 1]`: Each element in the array is a time period.
   The first element is $t = 0$ for the initial values of $m$ and $C$, the 2nd element
   is $t = 1$ and so on. Each vector is $[m_t, C_t, a_t, R_t, e, Q^{-1}]$,
   where matrices are converted to vecctors as column-major using `to_vector`.
   The indices of these elements are:

   - $[1, p]$: $m_t$
   - $[p + 1, p + p ^ 2]$: $C_t$
   - $[p + p^2 + 1, 2 p + p^2]$: $a_t$
   - $[2p + p^2 + 1, 2 p + 2p^2]$: $R_t$
   - $2p + 2p^2 + 1$: $f_t$
   - $2p + 2p^2 + 2$: $Q^{-1}_t$

  Each vector is $2 p^2 + 2 p + 2$ in

  */
  vector[] dlm_filter(int n, int r, int p,
                        vector[] y, int[,] miss,
                        vector[] b, matrix[] F, vector[] V,
                        vector[] g, matrix[] G, matrix[] W,
                        vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    vector[r] Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    vector[r] e;
    // Kalman gain
    vector[p] K;
    // returned data
    vector[dlm_filter_return_size(r, p)] res[n + 1];
    vector[p * p] C_vec;
    vector[p * p] R_vec;

    m <- m0;
    C <- C0;
    res[1] <- rep_vector(0.0, dlm_filter_return_size(r, p));
    for(i in 1:p) {
      res[1, i] <- m[i];
    }
    C_vec <- to_vector(C);
    for (i in 1:(p * p)) {
      res[1, p + i] <- C_vec[i];
    }
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- quad_form(C, G[t] ') + W[t];
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          e[j] <- 0.0;
          Q_inv[j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv[j] <- 1.0 / Q;
          // forecast error
          e[j] <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv[j];
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e[j];
          C <- make_symmetric_matrix(C - K * Q * K ');
        }
      }
      for(i in 1:p) {
        res[t + 1, i] <- m[i];
      }
      C_vec <- to_vector(C);
      for (i in 1:(p * p)) {
        res[t + 1, p + i] <- C_vec[i];
      }
      for(i in 1:p) {
        res[t + 1, p + p * p + i] <- a[i];
      }
      R_vec <- to_vector(R);
      for (i in 1:(p * p)) {
        res[t + 1, 2 * p + p * p + i] <- R_vec[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + i] <- e[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + r + i] <- Q_inv[i];
      }
    }
    return res;
  }


  vector[] dlm_constant_filter(int n, int r, int p,
                               vector[] y, int[,] miss,
                               vector b, matrix F, vector V,
                               vector g, matrix G, matrix W,
                               vector m0, matrix C0) {
    vector[r] b_tv[n];
    matrix[r, p] F_tv[n];
    vector[r] V_tv[n];
    vector[p] g_tv[n];
    matrix[p, p] G_tv[n];
    matrix[p, p] W_tv[n];

    b_tv <- rep_array(b, n);
    F_tv <- rep_array(F, n);
    V_tv <- rep_array(V, n);  
    g_tv <- rep_array(g, n);
    G_tv <- rep_array(G, n);
    W_tv <- rep_array(W, n);  
    return dlm_filter(n, r, p, y, miss,
                      b_tv, F_tv, V_tv,
                      g_tv, G_tv, W_tv,
                      m0, C0);
  }


  vector[] dlm_loglik(int n, int r, int p,
                        vector[] y, int[,] miss,
                        vector[] b, matrix[] F, vector[] V,
                        vector[] g, matrix[] G, matrix[] W,
                        vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    real Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    real e;
    // Kalman gain
    vector[p] K;
    // Loglik
    vector[r] loglik[n];

    m <- m0;
    C <- C0;
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- quad_form(C, G[t] ') + W[t];
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          loglik[t, j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv <- 1.0 / Q;
          // forecast error
          e <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv;
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e;
          C <- make_symmetric_matrix(C - K * Q * K ');
          loglik[t, j] <- - 0.5 * (log(Q) + e ^ 2 * Q_inv);
        }
      }
    }
    return loglik;
  }

  vector[] dlm_constant_loglik(int n, int r, int p,
                               vector[] y, int[,] miss,
                               vector b, matrix F, vector V,
                               vector g, matrix G, matrix W,
                               vector m0, matrix C0) {
    vector[r] b_tv[n];
    matrix[r, p] F_tv[n];
    vector[r] V_tv[n];
    vector[p] g_tv[n];
    matrix[p, p] G_tv[n];
    matrix[p, p] W_tv[n];

    b_tv <- rep_array(b, n);
    F_tv <- rep_array(F, n);
    V_tv <- rep_array(V, n);  
    g_tv <- rep_array(g, n);
    G_tv <- rep_array(G, n);
    W_tv <- rep_array(W, n);  
    return dlm_loglik(n, r, p, y, miss,
                      b_tv, F_tv, V_tv,
                      g_tv, G_tv, W_tv,
                      m0, C0);
  }


  // directly increment log-probability without returning the individual log-likelihoods
  real dlm_log(int n, int r, int p,
                  vector[] y, int[,] miss,
                  vector[] b, matrix[] F, vector[] V,
                  vector[] g, matrix[] G, matrix[] W,
                  vector m0, matrix C0) {
    vector[r] llall[n];
    vector[n] llobs;
    real ll;
    llall <- dlm_loglik(n, r, p, y, miss,
                     b, F, V,
                     g, G, W,
                     m0, C0);
    for (i in 1:n) {
      llobs[i] <- sum(llall[i]);
    }
    ll <- sum(llobs);
    return ll;
  }

  real dlm_constant_log(int n, int r, int p,
                        vector[] y, int[,] miss,
                        vector b, matrix F, vector V,
                        vector g, matrix G, matrix W,
                        vector m0, matrix C0) {
    vector[r] b_tv[n];
    matrix[r, p] F_tv[n];
    vector[r] V_tv[n];
    vector[p] g_tv[n];
    matrix[p, p] G_tv[n];
    matrix[p, p] W_tv[n];

    b_tv <- rep_array(b, n);
    F_tv <- rep_array(F, n);
    V_tv <- rep_array(V, n);  
    g_tv <- rep_array(g, n);
    G_tv <- rep_array(G, n);
    W_tv <- rep_array(W, n);  
    return dlm_log(n, r, p, y, miss,
                   b_tv, F_tv, V_tv,
                   g_tv, G_tv, W_tv,
                   m0, C0);
  }


  void dlm_lp(int n, int r, int p, 
                  vector[] y, int[,] miss,
                  vector[] b, matrix[] F, vector[] V,
                  vector[] g, matrix[] G, matrix[] W,
                  vector m0, matrix C0) {
    increment_log_prob(dlm_log(n, r, p, y, miss, b, F, V, g, G, W, m0, C0));
  }


  void dlm_constant_lp(int n, int r, int p,
                        vector[] y, int[,] miss,
                        vector b, matrix F, vector V,
                        vector g, matrix G, matrix W,
                        vector m0, matrix C0) {
    vector[r] b_tv[n];
    matrix[r, p] F_tv[n];
    vector[r] V_tv[n];
    vector[p] g_tv[n];
    matrix[p, p] G_tv[n];
    matrix[p, p] W_tv[n];

    b_tv <- rep_array(b, n);
    F_tv <- rep_array(F, n);
    V_tv <- rep_array(V, n);  
    g_tv <- rep_array(g, n);
    G_tv <- rep_array(G, n);
    W_tv <- rep_array(W, n);  
    dlm_lp(n, r, p, y, miss,
           b_tv, F_tv, V_tv,
           g_tv, G_tv, W_tv,
           m0, C0);
  }


  vector dlm_get_m(int t, int r, int p, vector[] v) {
    vector[p] x;
    x <- segment(v[t + 1], 1, p);
    return x;
  }


  vector[] dlm_get_m_all(int n, int r, int p, vector[] v) {
    vector[p] res[n + 1];
    for (t in 0:n) {
      res[t + 1] <- dlm_get_m(t, r, p, v);
    }
    return res;
  }


  matrix dlm_get_C(int t, int r, int p, vector[] v) {
    vector[p * p] x;
    matrix[p, p] res;
    x <- segment(v[t + 1], p + 1, p * p);
    res <- to_matrix_wdim(x, p, p);
    return res;
  }


  matrix[] dlm_get_C_all(int n, int r, int p, vector[] v) {
    matrix[p, p] res[n + 1];
    for (t in 0:n) {
      res[t + 1] <- dlm_get_C(t, r, p, v);
    }
    return res;
  }


  vector dlm_get_a(int t, int r, int p, vector[] v) {
    vector[p] res;
    res <- segment(v[t + 1], p + p * p + 1, p);
    return res;
  }


  vector[] dlm_get_a_all(int n, int r, int p, vector[] v) {
    vector[p] res[n];
    for (t in 1:n) {
      res[t] <- dlm_get_a(t, r, p, v);
    }
    return res;
  }


  matrix dlm_get_R(int t, int r, int p, vector[] v) {
    vector[p * p] x;
    matrix[p, p] res;
    x <- segment(v[t + 1], 2 * p + p * p + 1, p * p);
    res <- to_matrix_wdim(x, p, p);
    return res;
  }


  matrix[] dlm_get_R_all(int n, int r, int p, vector[] v) {
    matrix[p, p] res[n];
    for (t in 1:n) {
      res[t] <- dlm_get_R(t, r, p, v);
    }
    return res;
  }


  vector dlm_get_e(int t, int r, int p, vector[] v) {
    vector[r] res;
    res <- segment(v[t + 1], 2 * p + 2 * p * p + 1, r);
    return res;
  }


  vector[] dlm_get_e_all(int n, int r, int p, vector[] v) {
    vector[r] res[n];
    for (t in 1:n) {
      res[t] <- dlm_get_e(t, r, p, v);
    }
    return res;
  }


  vector dlm_get_Q_inv(int t, int r, int p, vector[] v) {
    vector[r] res;
    res <- segment(v[t + 1], 2 * p + 2 * p * p + r + 1, r);
    return res;
  }


  vector[] dlm_get_Q_inv_all(int n, int r, int p, vector[] v) {
    vector[r] res[n];
    for (t in 1:n) {
      res[t] <- dlm_get_Q_inv(t, r, p, v);
    }
    return res;
  }

  vector[] dlm_filter_loglik(int n, int r, int p, vector[] v, int[,] miss) {
    vector[r] ll[n];
    vector[r] err;
    vector[r] Q_inv;
    for (i in 1:n) {
      err <- dlm_get_e(i, r, p, v);
      Q_inv <- dlm_get_Q_inv(i, r, p, v);
      for (j in 1:r) {
        if (int_step(miss[i, j])) {
          ll[i, j] <- 0.0;
        } else {
          ll[i, j] <-  - 0.5 * (- log(Q_inv[j]) + (err[j] ^ 2) * Q_inv[j]);
        }
      }
    }
    return ll;
  }

  /*
  Backward sampling given results of a Kalman Filter

  - int n: Number of observations
  - int p: Number of states
  - matrix[p, p] G[n]: Transition matrices
  - vector[p] m[n + 1]: Mean of filtered states $p(\theta_t | y_1, \dots, y_t)$.
    Note that there are $n + 1$ entries, because $i = 1$ corresponds to the initial
    values at $t = 0$.
  - matrix[p, p] C[n + 1]: Covariance of filtered states $Cov(\theta_t | y_1, \dots, y_{t - 1})$.
  - vector[p] a[n]: Mean of predicted states $p(\theta_t \ y_1, \dots, y_t)$.
  - matrix[p, p] R[n]: Covariance of predicted states $Cov(\theta_t | y_t, \dots, y_{t - 1})$.

  */
  vector[] dlm_bsample_rng(int n, int p,
                                    matrix[] G,
                                    vector[] m, matrix[] C,
                                    vector[] a, matrix[] R) {
    vector[p] theta[n + 1];
    vector[p] h;
    matrix[p, p] H;
    matrix[p, p] R_inv;
    // TODO: sample once and cholesky transform

    // set last state
    theta[n + 1] <- multi_normal_rng(m[n + 1], C[n + 1]);
    for (i in 0:(n - 1)) {
       int t;
       t <- n - i;
       R_inv <- inverse(R[t]);
       h <- m[t] + C[t] * G[t] ' * R_inv  * (theta[t + 1] - a[t]);
       H <- make_symmetric_matrix(C[t] - C[t] * quad_form(R_inv, G[t]) * C[t]);
       theta[t] <- multi_normal_rng(h, H);
    }
    return theta;
  }

  vector[] dlm_filter_bsample_rng(int n, int r, int p, 
                                  matrix[] G,
                                  vector[] v) {
    vector[p] theta[n + 1];
    vector[p] h;
    matrix[p, p] H;
    matrix[p, p] R_inv;
    vector[p] a;
    matrix[p, p] R;
    vector[p] m;
    matrix[p, p] C;
    matrix[p, p] G_t;

    // TODO: sample once and cholesky transform
    // set last state
    m <- dlm_get_m(n, r, p, v);
    C <- dlm_get_C(n, r, p, v);
    theta[n + 1] <- multi_normal_rng(m, C);
    for (i in 1:n) {
       int t;
       t <- n - i;
       m <- dlm_get_m(t, r, p, v);
       C <- dlm_get_C(t, r, p, v);
       a <- dlm_get_a(t + 1, r, p, v);
       R <- dlm_get_R(t + 1, r, p, v);
       G_t <- G[t + 1];

       R_inv <- inverse(R);
       h <- m + C * G_t ' * R_inv  * (theta[t + 2] - a);
       H <- make_symmetric_matrix(C - C * quad_form(R_inv, G_t) * C);
       theta[t + 1] <- multi_normal_rng(h, H);
    }
    return theta;
  }


  // smoother
  vector[] dlm_smooth(int n, int p,
                      matrix[] G,
                      vector[] m, matrix[] C,
                      vector[] a, matrix[] R) {
    vector[p] theta[n + 1];
    vector[p] s;
    matrix[p, p] S;
    vector[p * p] S_vec;
    matrix[p, p] R_inv;
    // TODO: check dim;
    vector[p * p + p] ret[n + 1];

    // set last state
    s <- m[n + 1];
    S <- C[n + 1];
    for (i in 1:p) {
      ret[n + 1, i] <- s[i];
    }
    S_vec <- to_vector(S);
    for (i in 1:(p * p)) {
      ret[n + 1, p + i] <- S_vec[i];
    }
    for (i in 0:(n - 1)) {
       int t;
       t <- n - i;
       R_inv <- inverse(R[t]);
       s <- m[t] + C[t] * G[t] ' * R_inv  * (s - a[t]);
       S <- make_symmetric_matrix(C[t] - C[t] * G[t] ' * (R[t] - S) * R_inv * G[t] * C[t]);
       for (j in 1:p) {
         ret[t, j] <- s[i];
       }
       S_vec <- to_vector(S);
       for (j in 1:(p * p)) {
         ret[t, p + j] <- S_vec[i];
       }
    }
    return ret;
  }

  vector dlm_smooth_get_s(int t, int p, vector[] v) {
    vector[p] ret;
    ret <- segment(v[t + 1], 1, p);
    return ret;
  }

  vector[] dlm_smooth_get_s_all(int n, int p, vector[] v) {
    vector[p] ret[n + 1];
    for (t in 0:n) {
       ret[t + 1] <- dlm_smooth_get_s(t, p, v);
    }
    return ret;
  }

  matrix dlm_smooth_get_S(int t, int p, vector[] v) {
    vector[p * p] x;
    matrix[p, p] res;
    x <- segment(v[t + 1], p + 1, p * p);
    res <- to_matrix_wdim(x, p, p);
    return res;
  }

  matrix[] dlm_smooth_get_S_all(int n, int p, vector[] v) {
    matrix[p, p] res[n + 1];
    for (t in 0:n) {
      res[t + 1] <- dlm_smooth_get_S(t, p, v);
    }
    return res;
  }



  // DLM calculated with discounting
  vector[] dlm_discount_filter(int n, int r, int p,
                               vector[] y, int[,] miss,
                               vector[] b, matrix[] F, vector[] V,
                               vector[] g, matrix[] G, vector[] d,
                               vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    vector[r] Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    vector[r] e;
    // Kalman gain
    vector[p] K;
    // returned data
    vector[dlm_filter_return_size(r, p)] res[n + 1];
    vector[p * p] C_vec;
    vector[p * p] R_vec;

    m <- m0;
    C <- C0;
    res[1] <- rep_vector(0.0, dlm_filter_return_size(r, p));
    for(i in 1:p) {
      res[1, i] <- m[i];
    }
    C_vec <- to_vector(C);
    for (i in 1:(p * p)) {
      res[1, p + i] <- C_vec[i];
    }
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- diag_pre_multiply(1.0 ./ d[t], quad_form(C, G[t] '));
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          e[j] <- 0.0;
          Q_inv[j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv[j] <- 1.0 / Q;
          // forecast error
          e[j] <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv[j];
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e[j];
          C <- make_symmetric_matrix(C - K * Q * K ');
        }
      }
      for(i in 1:p) {
        res[t + 1, i] <- m[i];
      }
      C_vec <- to_vector(C);
      for (i in 1:(p * p)) {
        res[t + 1, p + i] <- C_vec[i];
      }
      for(i in 1:p) {
        res[t + 1, p + p * p + i] <- a[i];
      }
      R_vec <- to_vector(R);
      for (i in 1:(p * p)) {
        res[t + 1, 2 * p + p * p + i] <- R_vec[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + i] <- e[i];
      }
      for (i in 1:r) {
        res[t + 1, 2 * p + 2 * p * p + r + i] <- Q_inv[i];
      }
    }
    return res;
  }


  vector[] dlm_discount_loglik(int n, int r, int p, 
                               vector[] y, int[,] miss,
                               vector[] b, matrix[] F, vector[] V,
                               vector[] g, matrix[] G, vector[] d,
                               vector m0, matrix C0) {
    // prior of state: p(theta | y_t, ..., y_{t-1})
    vector[p] a;
    matrix[p, p] R;
    // likelihood of obs: p(y | y_t, ..., y_t-1)
    vector[p] Fj;
    real f;
    real Q;
    real Q_inv;
    // posterior of states: p(theta_t | y_t, ..., y_t)
    vector[p] m;
    matrix[p, p] C;
    // forecast error
    real e;
    // Kalman gain
    vector[p] K;
    // Loglik
    vector[r] loglik[n];

    m <- m0;
    C <- C0;
    for (t in 1:n) {
      // PREDICT STATES
      // one step ahead predictive distribion of p(\theta_t | y_{1:(t-1)})
      a <- g[t] + G[t] * m;
      R <- diag_pre_multiply(1.0 ./ d[t], quad_form(C, G[t] '));
      m <- a;
      C <- R;
      for (j in 1:r) {
        if (int_step(miss[t, j])) {
          loglik[t, j] <- 0.0;
        } else {
          // PREDICT OBS
          // one step ahead predictive distribion of p(y_t | y_{1:(t-1)})
          Fj <- F[t, j] ';
          f <- b[t, j] + dot_product(Fj, m);
          Q <- quad_form(C, Fj) + V[t, j];
          Q_inv <- 1.0 / Q;
          // forecast error
          e <- y[t, j] - f;
          // Kalman gain
          K <- C * Fj * Q_inv;
          // FILTER STATES
          // posterior distribution of p(\theta_t | y_{1:t})
          m <- m + K * e;
          C <- make_symmetric_matrix(C - K * Q * K ');
          loglik[t, j] <- - 0.5 * (log(Q) + e ^ 2 * Q_inv);
        }
      }
    }
    return loglik;
  }

  real dlm_discount_log(int n, int r, int p, 
                  vector[] y, int[,] miss,
                  vector[] b, matrix[] F, vector[] V,
                  vector[] g, matrix[] G, vector[] d,
                  vector m0, matrix C0) {
    vector[r] llall[n];
    vector[n] llobs;
    real ll;
    llall <- dlm_discount_loglik(n, r, p, y, miss,
                                 b, F, V,
                                 g, G, d,
                                 m0, C0);
    for (i in 1:n) {
      llobs[i] <- sum(llall[i]);
    }
    ll <- sum(llobs);
    return ll;
  }


  void dlm_discount_lp(int n, int r, int p, 
                  vector[] y, int[,] miss,
                  vector[] b, matrix[] F, vector[] V,
                  vector[] g, matrix[] G, vector[] d,
                  vector m0, matrix C0) {
    increment_log_prob(dlm_discount_log(n, r, p, y, miss, b, F, V, g, G, d, m0, C0));
  }




  // --- END: dlm ---
}
data {
  // dimensions
  int n; // number of observations / times
  int p; // number of states
  int r; // number of variables
  // observations
  vector[r] y[n];
  int y_miss[n, r];
  vector[n] y_diff;

  // initial conditions
  vector[p] m0;
  cov_matrix[p] C0;

  // Battles
  // number of battles
  int n_union;
  int n_union_obs;
  int<lower = 1, upper = n_union> union_battle[n_union_obs];
  int<lower = 1, upper = n> union_times[n_union_obs];
  vector<lower = 0, upper = 1>[n_union_obs] union_wgt;

  int n_confed;
  int n_confed_obs;
  int<lower = 1, upper = n_confed> confed_battle[n_confed_obs];
  int<lower = 1, upper = n> confed_times[n_confed_obs];
  vector<lower = 0, upper = 1>[n_confed_obs] confed_wgt;
  
  // prior params
  real<lower = 0.0> nu_theta;
  real<lower = 0.0> tau_scale;
  real<lower = 0.0> sigma_scale;
  real<lower = 0.0> zeta_scale;
  real<lower = 0.0> rho_a;
  real<lower = 0.0> rho_b;
}
transformed data {
  vector[r] b_tv[n];
  matrix[r, p] F_tv[n];
  matrix[p, p] G_tv[n];
  
  b_tv <- rep_array(rep_vector(0.0, r), n);
  F_tv <- rep_array(rep_matrix(1.0, r, p), n);
  G_tv <- rep_array(rep_matrix(1.0, p, p), n); 
}
parameters {
  real<lower = 0.0> sigma;
  real<lower = 0.0> tau;
  vector<lower = 0.0>[n] lambda_theta;

  real alpha;
  
  real beta_C;
  vector[n_confed] gamma_C;
  real<lower = 0.0> zeta;
  real<lower = 0.0, upper = 1.0> rho;  
  
  real beta_U;
  vector[n_union] gamma_U;

}
transformed parameters {
  // log-likelihood
  vector[r] loglik[n];
  matrix[p, p] W[n];
  vector[n] delta_battle;
  vector[n_confed] omega_C;
  vector[n_union] omega_U;
  vector[2 * p * p + 2 * p + 2 * r] dlm_res[n + 1];


  // total battle effects
  omega_U[1] <- beta_U + zeta * gamma_U[1];
  for (i in 2:n_union) {
    omega_U[i] <- rho * omega_U[i - 1] + (1.0 - rho) * beta_U + sqrt(rho) * zeta * gamma_U[i];
  }
  omega_C[1] <- beta_C + zeta * gamma_C[1];
  for (i in 2:n_confed) {
    omega_C[i] <- rho * omega_C[i - 1] + (1.0 - rho) * beta_C + sqrt(rho) * zeta * gamma_C[i];
  }
  // update
  delta_battle <- rep_vector(0.0, n);
  for (i in 1:n_confed_obs) {  
    delta_battle[confed_times[i]] <- delta_battle[confed_times[i]] + omega_C[confed_battle[i]] * confed_wgt[i];    
  }
  for (i in 1:n_union_obs) {
    delta_battle[union_times[i]] <- delta_battle[union_times[i]] + omega_U[union_battle[i]] * union_wgt[i];
  }
  for (i in 1:n) {
    W[i] <- rep_matrix(lambda_theta[i] * pow(sigma, 2) * pow(tau, 2) * y_diff[i], p, p);
  }
  {
    vector[r] V[n];
    vector[p] g_tv[n];

    for (i in 1:n) {
      g_tv[i] <- rep_vector(alpha * y_diff[i] + delta_battle[i], r);
    }
    V <- rep_array(rep_vector(pow(tau, 2), r), n);
    dlm_res <- dlm_filter(n, r, p,
                          y, y_miss,
                          b_tv, F_tv, V,
                          g_tv, G_tv, W,
                          m0, C0);
    loglik <- dlm_filter_loglik(n, r, p, dlm_res, y_miss);
  }
}
model {
  vector[n] ll;
  
  // BATTLES
  zeta ~ cauchy(0.0, zeta_scale);
  rho ~ beta(rho_a, rho_b);
  gamma_U ~ normal(0.0, 1.0);
  gamma_C ~ normal(0.0, 1.0);
  
  // OBS EQUATION
  // bound measurement error away from 0
  // gamma(2, lambda) with lambda -> 0.
  // Chung et a. 2013, See Stan Modeling Guide on boundary avoiding priors.
  tau ~ gamma(2.0, 1.0 / tau_scale);
  // SYSTEM EQUATION
  sigma ~ cauchy(0.0, sigma_scale);
  lambda_theta ~ inv_gamma(0.5 * nu_theta, 0.5 * nu_theta);
  for (i in 1:n) {
    ll[i] <- loglik[i, 1];
  }
  increment_log_prob(sum(ll));
}
generated quantities {
  vector[p] theta[n + 1];
  // likelihood of obs: p(y | y_t, ..., y_t-1)
  vector[r] err[n];
  vector[r] Q_inv[n];
  // likelihood of obs: p(y | y_t, ..., y_t-1)
  vector[p] m[n + 1];
  vector[n] cum_delta_battle;

  cum_delta_battle <- cumulative_sum(delta_battle);
  err <- dlm_get_e_all(n, r, p, dlm_res);
  Q_inv <- dlm_get_Q_inv_all(n, r, p, dlm_res);
  m <- dlm_get_m_all(n, r, p, dlm_res);  
  theta <- dlm_filter_bsample_rng(n, r, p, G_tv, dlm_res);
}
