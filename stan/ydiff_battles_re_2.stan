data {
  // number of y observations
  int time_max;
  // observed data
  vector[time_max] y;
  // time since last period
  vector<lower = 1>[time_max] diff;
  // Battles
  // number of battles
  int n_battles;
  int n_battle_groups;
  int<lower = 1, upper = n_battle_groups> battle_group[n_battles];
  // battle observations can be split across times
  int n_battle_obs;
  int<lower = 1, upper = n_battles> battle[n_battle_obs];
  int<lower = 1, upper = time_max> battle_times[n_battle_obs];
  vector<lower = 0, upper = 1>[n_battle_obs] battle_obs_wgt;
  // Priors
  real<lower = 0.0> sigma_scale;
  real<lower = 0.0> zeta_scale;
  vector[n_battles] skew_direction;
  real<lower = 0.0> skew_coef;
  // Observation df
  real<lower = 2.0> y_obs_df;
}
parameters {
  // sigma innovation
  real<lower = 0.0> sigma;
  // intercept and coefficients
  real alpha;
  real<lower = 0.0> beta_skew;

  vector[n_battles] beta_1;
  real<lower = 0.0> beta_2;
  vector[n_battle_groups] gamma;
  real<lower = 0.0> zeta;
  real<lower = 3.0> nu;

}
transformed parameters {
  vector[time_max] mu;
  vector[time_max] y_scale;
  vector[n_battles] beta;

  for(i in 1:n_battles) {
    beta[i] <- gamma[battle_group[i]]
    + (zeta * sigma) * beta_1[i] / sqrt(beta_2);
  }

  mu <- rep_vector(alpha, time_max);
  for (i in 1:n_battle_obs) {
    mu[battle_times[i]] <- mu[battle_times[i]]
      + beta[battle[i]] * battle_obs_wgt[i];
  }
  for (i in 1:time_max) {
    y_scale[i] <- sigma * sqrt(diff[i]);
  }
}
model {
  sigma ~ cauchy(0.0, sigma_scale);
  y ~ student_t(y_obs_df, mu, y_scale);
  beta_1 ~ skew_normal(0.0, 1.0, skew_direction * beta_skew);
  beta_2 ~ gamma(nu / 2.0, nu / 2.0);
  nu ~ gamma(2, 0.1);
  zeta ~ cauchy(0, zeta_scale);
  beta_skew ~ normal(0, skew_coef);
}
generated quantities {
  vector[time_max] log_lik;
  vector[time_max] err;
  real log_lik_total;

  for (i in 1:time_max) {
    log_lik[i] <- student_t_log(y[i], y_obs_df, mu[i], sigma);
    err[i] <- y[i] - mu[i];
  }
  log_lik_total <- sum(log_lik);

}

