data {
  // number of y observations
  int time_max;
  // observed data
  vector[time_max] y;
  // time since last period
  vector<lower = 1>[time_max] diff;
  // Priors
  real<lower = 0.0> sigma_scale;
  // observation df
  real<lower = 2.0> y_obs_df;
}
parameters {
  // sigma innovation
  real<lower = 0.0> sigma;
  // intercept and coefficients
  real mu;
}
transformed parameters {
  vector[time_max] y_scale;

  for (i in 1:time_max) {
    y_scale[i] <- sigma * sqrt(diff[i]);
  }
}
model {
  sigma ~ cauchy(0.0, sigma_scale);
  y ~ student_t(y_obs_df, mu, y_scale);
}
generated quantities {
  vector[time_max] log_lik;
  vector[time_max] err;
  real log_lik_total;

  for (i in 1:time_max) {
    log_lik[i] <- student_t_log(y[i], y_obs_df, mu, sigma);
    err[i] <- y[i] - mu;
  }
  log_lik_total <- sum(log_lik);

}

