format_date_range <- Vectorize(function(start_date, end_date, abbr = FALSE) {
  y1 <- year(start_date)
  y2 <- year(end_date)
  m1 <- month(start_date)
  m2 <- month(end_date)
  d1 <- day(start_date)
  d2 <- day(end_date)
  if (y1 == y2) {
    if (m1 == m2) {
      if (d1 == d2) {
        sprintf("%s %d, %d", month(start_date, TRUE, abbr), d1, y1)
      } else {
        sprintf("%s %d--%d, %d",
               month(start_date, TRUE, abbr), d1, d2, y1)
      }
    } else {
      sprintf("%s %d--%s %d, %d",
             month(start_date, TRUE, abbr), d1,
             month(end_date, TRUE, abbr), d2, y1)
    }
  } else {
    sprintf("%s %d, %d--%s %d, %d",
           month(start_date, TRUE, abbr), m1, m2,
           month(end_date, TRUE, abbr), m1, m2
           )
  }
})


#' Battle news data
#'
#' Convert to a dataset with all dates, and weights equal to 1 / number of days.
#'
#' Returns a data frame with columns
#'
#' - name of the `.di` variable
#' - date : date
#' - wgt : weight associated with that date
#'
create_wgts_from_dates <- function(.data, lag, start_date_var, end_date_var, .id) {
  .data <- .data[ , c(.id, start_date_var, end_date_var)]
  lapply(split(.data, seq_len(nrow(.data))),
         function(x) {
           ret <- data_frame(date = seq(x[[start_date_var]], x[[end_date_var]] + lag, by = 1),
                             wgt = 1 / length(date))
           ret[[.id]] <- x[[.id]]
           ret
         }) %>% bind_rows()
}

