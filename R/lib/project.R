Project <- R6Class("Project",
  public = list(
    root_dir = NA,
    db_dir = NA,
    db = NULL,
    init_db = NULL,
    init_db_dir = NA,
    initialize = function(root_dir  = ".", db_dir = "rdata", db_type = "RDS",
                          init_db_dir = "data/stan_init", init_db_type = "RDS") {
      self$root_dir <- tools::file_path_as_absolute(root_dir)
      self$db_dir <- self$path(db_dir)
      filehash::dbCreate(self$db_dir)
      self$db <- filehash::dbInit(self$db_dir, type = db_type)
      self$init_db_dir <- self$path(init_db_dir)
      filehash::dbCreate(self$init_db_dir, init_db_type)
      self$init_db <- filehash::dbInit(self$init_db_dir, init_db_type)
    },
    path = function(...) {
      file.path(self$root_dir, ...)
    },
    save_init = function(key, value) {
      filehash::dbInsert(self$init_db, key, value)
    },
    get_init = function(key) {
      if (key %in% names(self$init_db)) {
        filehash::dbFetch(self$init_db, key)
      } else {
        'random'
      }
    },
    resource = function(envir = parent.frame()) {
      browser()
      sys.source(self$path("R/lib/aaa.R"),
                 chdir = TRUE,
                 envir = envir)
    }
  )
)
