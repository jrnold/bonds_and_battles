stan_model5a_create_data <- function(proj) {

  within(list(), {
    START_DATE <- as.Date("1861-04-27")
    END_DATE <- as.Date("1865-05-04")
    SERIES <- c("U.S. 5's, 1874")
    # From inspection of weird dates
    BAD_DATES <- as.Date(c("1864-03-22"))
    LAG <- 5

    prices <-
      proj$db[["prices1"]] %>%
      filter(series %in% SERIES,
             date >= START_DATE, date <= END_DATE,
             ! date %in% BAD_DATES) %>%
      mutate(log_yield = log(yield)) %>%
      group_by(date) %>%
      mutate(n = n()) %>%
      filter(n == 1 | src == "Bankers") %>%
      arrange(date) %>%
      ungroup() %>%
      mutate(lag_date = lag(date),
             diff = as.integer(difftime(date, lag(date), units = "days")),
             y = log_yield,
             miss = 0L,
             time = seq_along(date))
    prices[1, "lag_date"] <- prices[1, "date"] - 1L
    prices[1, "diff"] <- 1L

    date_dict <-
      prices %>%
      rowwise() %>%
      do({
        data_frame(date_from = seq(.$lag_date + 1L, .$date, by = 1),
                   date_to = .$date,
                   time = .$time)
      })

    EXCLUDED_BATTLES <- c("SC001")
    battles <- PROJECT$db[["battles1"]] %>%
      filter(significance == "A",
             ! is.na(news_start_date),
             ! battle %in% EXCLUDED_BATTLES) %>%
      arrange(end_date, start_date) %>%
      `[<-`(.[["battle"]] == "VA046", "outcome", value = "Union") %>%
      `[<-`(.[["battle"]] == "VA048", "outcome", value = "Confederate")

    confed_battles <- battles %>% filter(outcome == "Confederate") %>%
      mutate(n_battle = seq_along(battle)) %>%
      select(n_battle, battle, battle_name_short,
             start_date, end_date,
             news_start_date, news_end_date)

    confed_battles_wgts <- confed_battles %>%
      create_wgts_from_dates(lag = LAG,
                             "news_start_date", "news_end_date",
                             "battle") %>%
      filter(date >= START_DATE,
             date <= END_DATE) %>%
      left_join(date_dict, by = c(date = "date_from")) %>%
      group_by(battle, date_to, time) %>%
      summarise(wgt = sum(wgt)) %>%
      left_join(select(confed_battles, battle, n_battle), by = "battle")

    union_battles <- battles %>% filter(outcome == "Union") %>%
      mutate(n_battle = seq_along(battle)) %>%
      select(n_battle, battle, battle_name_short,
             start_date, end_date,
             news_start_date, news_end_date)

    union_battles_wgts <- union_battles %>%
      create_wgts_from_dates(lag = LAG,
                             "news_start_date", "news_end_date",
                             "battle") %>%
      filter(date >= START_DATE,
             date <= END_DATE) %>%
      left_join(date_dict, by = c(date = "date_from")) %>%
      group_by(battle, date_to, time) %>%
      summarise(wgt = sum(wgt)) %>%
      left_join(select(union_battles, battle, n_battle), by = "battle")

    data <- within(list(), {
      p <- 1
      r <- 1
      n <- nrow(prices)
      y <- matrix(prices[["y"]])
      y_miss <- matrix(prices[["miss"]])
      y_diff <- c(1, prices[["diff"]][-1])
      m0 <- array(prices$y[1])
      C0 <- matrix(0.05)

      n_union <- nrow(union_battles)
      n_union_obs <- nrow(union_battles_wgts)
      union_battle <- union_battles_wgts[["n_battle"]]
      union_times <- union_battles_wgts[["time"]]
      union_wgt <- union_battles_wgts[["wgt"]]

      n_confed <- nrow(confed_battles)
      n_confed_obs <- nrow(confed_battles_wgts)
      confed_battle <- confed_battles_wgts[["n_battle"]]
      confed_times <- confed_battles_wgts[["time"]]
      confed_wgt <- confed_battles_wgts[["wgt"]]

      nu_theta <- 5
      tau_scale <- 0.05
      sigma_scale <- 1
      zeta_scale <- 0.05
      rho_a <- 1
      rho_b <- 1
    })
  })
}
