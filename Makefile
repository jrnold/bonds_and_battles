R_FILES = $(wildcard R/db_*.R)

DB_DIR = rdata
DB_FILES = $(R_FILES:R/db_%.R=$(DB_DIR)/%.rda)
# create database objects

data: $(DB_FILES)

$(DB_DIR)/%: R/db/%.R
	RScript $< $(notdir $@)

# push/pull data
S3_BUCKET = jrnold-data-1
S3_BUCKET_PREFIX = bonds-and-battles

pull-data:
	aws s3 sync s3://$(S3_BUCKET)/$(S3_BUCKET_PREFIX)/$(DB_DIR) $(DB_DIR)

push-data:
	aws s3 sync $(DB_DIR) s3://$(S3_BUCKET)/$(S3_BUCKET_PREFIX)/$(DB_DIR) --acl public-read --delete

# update submodules
update:
	git submodule update --remote --recursive --init

.PHONY: update
